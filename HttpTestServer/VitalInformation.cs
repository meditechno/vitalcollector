﻿///////////////////////////////////////////////////////
// Copyright (c) 2020 PHC株式会社
// Filename: VitalInformation.cs
///////////////////////////////////////////////////////

namespace HttpTestServer
{
    using System;
    using System.Globalization;
    using System.IO;
    using System.Net;
    using System.Web;

    /// <summary>
    /// バイタル情報登録受付用クラス。
    /// </summary>
    public class VitalInformation
    {
        private const string ProjectCode = "L01A01_002020_01";
        private const string PatientCode1 = "99100001";
        private const string PatientCode2 = "99900062";
        private const string PatientCode3 = "99100003";
        private const string PatientCode4 = "99900063";
        private const string StaffCode = "9004";

        public string プロジェクトcd { set; get; }
        public string 処理区分 { get; set; }
        public string 患者番号 { get; set; }
        public string 計測日 { get; set; }
        public string 計測時間 { get; set; }
        public string 身長 { get; set; }
        public string 体重 { get; set; }
        public string 体温 { get; set; }
        public string 脈拍 { get; set; }
        public string 呼吸数 { get; set; }
        public string 最高血圧 { get; set; }
        public string 最低血圧 { get; set; }
        public string 酸素飽和度 { get; set; }
        public string 記事記載区分 { get; set; }
        public string 登録者id { get; set; }

        public static VitalInformation ParseVitalInformation(HttpListenerRequest request)
        {
            if (!request.HasEntityBody)
            {
                return null;
            }

            using var body = request.InputStream;
            using var reader = new StreamReader(body, request.ContentEncoding);
            var bodydata = reader.ReadToEnd();
            var parameters = bodydata.Split("&");
            var vital = new VitalInformation();

            foreach (var item in parameters)
            {
                var pair = item.Split("=");
                var key = HttpUtility.UrlDecode(pair[0]);

                switch (key)
                {
                case nameof(プロジェクトcd):
                    vital.プロジェクトcd = pair[1];
                    break;
                case nameof(処理区分):
                    vital.処理区分 = pair[1];
                    break;
                case nameof(患者番号):
                    vital.患者番号 = pair[1];
                    break;
                case nameof(酸素飽和度):
                    vital.酸素飽和度 = pair[1];
                    break;
                case nameof(計測日):
                    vital.計測日 = pair[1];
                    break;
                case nameof(計測時間):
                    vital.計測時間 = pair[1];
                    break;
                case nameof(身長):
                    vital.身長 = pair[1];
                    break;
                case nameof(体重):
                    vital.体重 = pair[1];
                    break;
                case nameof(体温):
                    vital.体温 = pair[1];
                    break;
                case nameof(脈拍):
                    vital.脈拍 = pair[1];
                    break;
                case nameof(呼吸数):
                    vital.呼吸数 = pair[1];
                    break;
                case nameof(最高血圧):
                    vital.最高血圧 = pair[1];
                    break;
                case nameof(最低血圧):
                    vital.最低血圧 = pair[1];
                    break;
                case nameof(記事記載区分):
                    vital.記事記載区分 = pair[1];
                    break;
                case nameof(登録者id):
                    vital.登録者id = pair[1];
                    break;
                }
            }

            return vital;
        }

        public void ValidateVitalInformation()
        {
            if (プロジェクトcd != ProjectCode)
            {
                Console.WriteLine($" ！プロジェクトcd[{プロジェクトcd}]");
                throw new ArgumentException("error code 1001");
            }
            Console.WriteLine($" プロジェクトcd[{プロジェクトcd}]");

            if (処理区分 != "1" && 処理区分 != "2")
            {
                Console.WriteLine($" ！処理区分[{処理区分}]");
                throw new ArgumentException("error code 1002");
            }
            Console.WriteLine($" 処理区分[{処理区分}]");

            if (患者番号 != PatientCode1 &&
                患者番号 != PatientCode2 &&
                患者番号 != PatientCode3 &&
                患者番号 != PatientCode4)
            {
                Console.WriteLine($" ！患者番号[{患者番号}]");
                throw new ArgumentException("error code 1003");
            }
            Console.WriteLine($" 患者番号[{患者番号}]");

            if (!DateTime.TryParseExact($"{計測日} {計測時間}", "yyyyMMdd HHmmss", CultureInfo.InvariantCulture, DateTimeStyles.None, out _))
            {
                Console.WriteLine($" ！計測日[{計測日}]");
                Console.WriteLine($" ！計測時間[{計測時間}]");
                throw new ArgumentException("error code 1004");
            }
            Console.WriteLine($" 計測日[{計測日}]");
            Console.WriteLine($" 計測時間[{計測時間}]");

            if (身長 != null && !decimal.TryParse(身長, out _))
            {
                Console.WriteLine($" ！身長[{身長}]");
                throw new ArgumentException("error code 1005");
            }
            Console.WriteLine($" 身長[{身長}]");

            if (体重 != null && !decimal.TryParse(体重, out _))
            {
                Console.WriteLine($" ！体重[{体重}]");
                throw new ArgumentException("error code 1006");
            }
            Console.WriteLine($" 体重[{体重}]");

            if (体温 != null && !decimal.TryParse(体温, out _))
            {
                Console.WriteLine($" ！体温[{体温}]");
                throw new ArgumentException("error code 1007");
            }
            Console.WriteLine($" 体温[{体温}]");

            if (脈拍 != null && (脈拍?.Length > 3 || !int.TryParse(脈拍, out _)))
            {
                Console.WriteLine($" ！脈拍[{脈拍}]");
                throw new ArgumentException("error code 1008");
            }
            Console.WriteLine($" 脈拍[{脈拍}]");

            if (呼吸数 != null && (呼吸数?.Length > 3 || !int.TryParse(呼吸数, out _)))
            {
                Console.WriteLine($" ！呼吸数[{呼吸数}]");
                throw new ArgumentException("error code 1009");
            }
            Console.WriteLine($" 呼吸数[{呼吸数}]");

            if (最高血圧 != null && (最高血圧?.Length > 3 || !int.TryParse(最高血圧, out _)))
            {
                Console.WriteLine($" ！最高血圧[{最高血圧}]");
                throw new ArgumentException("error code 1010");
            }
            Console.WriteLine($" 最高血圧[{最高血圧}]");

            if (最低血圧 != null && (最低血圧?.Length > 3 || !int.TryParse(最低血圧, out _)))
            {
                Console.WriteLine($" ！最低血圧[{最低血圧}]");
                throw new ArgumentException("error code 1011");
            }
            Console.WriteLine($" 最低血圧[{最低血圧}]");

            if (酸素飽和度 != null && (酸素飽和度?.Length > 3 || !int.TryParse(酸素飽和度, out _)))
            {
                Console.WriteLine($" ！酸素飽和度[{酸素飽和度}]");
                throw new ArgumentException("error code 1012");
            }
            Console.WriteLine($" 酸素飽和度[{酸素飽和度}]");

            if (記事記載区分 != null && (記事記載区分?.Length > 1 || !int.TryParse(記事記載区分, out _)))
            {
                Console.WriteLine($" ！記事記載区分[{記事記載区分}]");
                throw new ArgumentException("error code 1013");
            }
            Console.WriteLine($" 記事記載区分[{記事記載区分}]");

            if (登録者id != null && 登録者id != StaffCode)
            {
                Console.WriteLine($" ！登録者id[{登録者id}]");
                throw new ArgumentException("error code 1013");
            }
            Console.WriteLine($" 登録者id[{登録者id}]");
        }
    }
}

