﻿///////////////////////////////////////////////////////
// Copyright (c) 2020 PHC株式会社
// Filename: Program.cs
///////////////////////////////////////////////////////

namespace HttpTestServer
{
    using System;
    using System.Net;
    using System.Text;

    public static class Program
    {
        private static void Main(string[] args)
        {
            if (args is null)
            {
                throw new ArgumentNullException(nameof(args));
            }

            try
            {
                var listener = new HttpListener();
                listener.Prefixes.Add("http://localhost:10234/");
                listener.Start();

                Console.WriteLine("HTTP service start");

                while (true)
                {
                    var context = listener.GetContext();
                    var request = context.Request;
                    var response = context.Response;

                    var urlPath = request.Url;
                    Console.WriteLine($"Request to: [{request.HttpMethod}]{urlPath}");

                    try
                    {
                        if (request.HttpMethod != "POST")
                        {
                            response.StatusCode = 403;
                        }
                        else
                        {
                            try
                            {
                                var vitalInformation = VitalInformation.ParseVitalInformation(request);
                                vitalInformation.ValidateVitalInformation();
                                response.StatusCode = 200;
                                Console.WriteLine("Request Success!");
                            }
                            catch (ArgumentException e)
                            {
                                response.StatusCode = 500;
                                var content = Encoding.UTF8.GetBytes(e.Message);
                                response.OutputStream.Write(content, 0, content.Length);
                            }
                            catch (NullReferenceException)
                            {
                                response.StatusCode = 500;
                                var content = Encoding.UTF8.GetBytes("メッセージ形式が不正です");
                                response.OutputStream.Write(content, 0, content.Length);
                            }
                        }
                    }
                    catch (Exception reqException)
                    {
                        Console.WriteLine($"{reqException.Message} {reqException.StackTrace}");
                        response.StatusCode = 500;
                    }
                    response.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"{ex.Message} {ex.StackTrace}");
            }
        }
    }
}
