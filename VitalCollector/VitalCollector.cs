﻿///////////////////////////////////////////////////////
// Copyright (c) 2020 PHC株式会社
// Filename: VitalCollector.cs
///////////////////////////////////////////////////////

namespace VitalCollector
{
    using global::VitalCollector.Common;
    using global::VitalCollector.Data;
    using global::VitalCollector.NihonKodenHL7;
    using HL7.Dotnetcore;
    using Microsoft.Extensions.Configuration;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Net.Sockets;
    using System.Text;

    public class VitalCollector
    {
        public IConfiguration Configuration { get; set; }

        public async System.Threading.Tasks.Task ExecuteAsync(bool execVitalCollector)
        {
            var recvCount = 0;
            var hl7successCount = 0;
            var bpsuccessCount = 0;
            var lwsuccessCount = 0;

            // 設定を読み込み
            var config = VitalCollectorConfig.ReadConfiguration(Configuration);
            var modeStr = execVitalCollector ? "ON" : "OFF";
            Logger.WriteLog($"VitalCollector起動 血圧集計[ON] バイタル集計[{modeStr}]");

            // 2重起動禁止
            try
            {
                PreventDuplicateInstances();
            }
            catch (Exception)
            {
                Logger.WriteLog("他のインスタンスが検知されました。二重起動は禁止されています");
                throw;
            }

            RegisterLiveWorks.Config = config;
            BloodPressureManager.LoadData();

            for (int gatewayIndex = 0; gatewayIndex < config.GatewayAddress.Count; gatewayIndex++)
            {
                using var client = new TcpClient();
                try
                {
                    Logger.WriteLog($"接続開始[{config.GatewayAddress[gatewayIndex]}:{config.GatewayPortNumber[gatewayIndex]}]");

                    if (!client.ConnectAsync(config.GatewayAddress[gatewayIndex], config.GatewayPortNumber[gatewayIndex]).Wait(config.GatewayConnectTimeout * 1000))
                    {
                        Logger.WriteLog($"サーバー接続タイムアウト[{config.GatewayConnectTimeout * 1000}]");
                        throw new InvalidOperationException("サーバーに到達できません");
                    }

                    using var stream = client.GetStream();

                    var personList = new List<Person>();
                    var bContinue = true;

                    while (recvCount < config.MaxPatientCount && bContinue)
                    {
                        // 返信を受信
                        var recvdata = new byte[8152];
                        var recvsize = stream.Read(recvdata, 0, recvdata.Length);

                        if (recvsize <= 0)
                        {
                            Logger.WriteLog("データを受信できませんでした");
                            break;
                        }

                        if (config.SaveHL7)
                        {
                            Logger.WriteBinaryToLogfile(recvdata, recvsize, Logger.Mode.GWRecv);
                        }

                        var recvBuffer = CheckAndExtractRecvHL7Message(recvdata);
                        if (recvBuffer == null)
                        {
                            Logger.WriteLog("データが無効です");
                            break;
                        }

                        var responseData = Encoding.ASCII.GetString(recvBuffer).Replace('\0', ' ').Trim();

                        // 解析
                        var message = new Message(responseData);
                        if (!message.ParseMessage())
                        {
                            // エラー
                            throw new ArgumentException("メッセージ解析エラー");
                        }

                        var response = new VitalCollectResponse();
                        try
                        {
                            response.ParseMessage(message);
                            var person = response.Results.Values.FirstOrDefault();

                            // 繰り返されていれば終了
                            if (personList.Any(x => x?.PV1Element?.BedInformation == person?.PV1Element?.BedInformation))
                            {
                                Logger.WriteLog("読み込みが終了しました");
                                bContinue = false;
                            }
                            else
                            {
                                personList.Add(person);
                                recvCount++;
                            }
                        }
                        catch (Exception e)
                        {
                            Logger.WriteLog($"Error Parsing Packet: {e.Message}");
                            recvCount++;
                        }

                        // 統計
                        if (bContinue)
                        {
                            hl7successCount += response.Results.Values.Count(x => x.HasData == true);
                        }

                        // ACKを返送
                        var packet = response.CreateAckMessage().SerializeMessage(false);

                        Logger.WriteLog($"SENDING ACK: {packet.Replace("\r", "\r\n")}");
                        var encodeddata = Encoding.ASCII.GetBytes(packet);
                        var senddata = WrapHL7HeaderTrailer(encodeddata);

                        if (config.SaveHL7)
                        {
                            Logger.WriteBinaryToLogfile(senddata, senddata.Length, Logger.Mode.GWSend);
                        }

                        stream.Write(senddata);
                    }

                    Logger.WriteLog($"読み込み件数[{recvCount}] 成功件数[{hl7successCount}]");

                    // 結果をPost
                    Logger.WriteLog($"LiveWorksに血圧登録開始 url[{config.LiveWorksUrl}] 登録閾値[{config.RegisterThresholdSec}]秒");
                    bpsuccessCount += await RegisterLiveWorks.RegisterBloodPressureAsync(config.LiveWorksUrl, personList).ConfigureAwait(false);
                    Logger.WriteLog("LiveWorksへの血圧登録終了");

                    // 血圧データを保存
                    BloodPressureManager.SaveData();

                    if (execVitalCollector)
                    {
                        Logger.WriteLog($"LiveWorksに登録開始 url[{config.LiveWorksUrl}] 登録閾値[{config.RegisterThresholdSec}]秒");
                        lwsuccessCount += await RegisterLiveWorks.RegisterVitalInfoAsync(config.LiveWorksUrl, config.RegisterThresholdSec, personList).ConfigureAwait(false);
                        Logger.WriteLog("LiveWorksへの登録終了");
                    }
                }
                catch (Exception e)
                {
                    Logger.WriteLog($"VitalCollectorエラー [{e.Message}]", e: e);
                }
            }

            Logger.WriteLog($"VitalCollector終了 HL7読込成功{hl7successCount}件 血圧登録成功{bpsuccessCount}件 バイタル登録成功{lwsuccessCount}件 失敗{recvCount - lwsuccessCount}件");

            Logger.RotateMainLog(config);
            Logger.RotateFileLog(config);
        }

        private byte[] WrapHL7HeaderTrailer(byte[] body)
        {
            var header = new byte[] { 0x0B };
            var trailer = new byte[] { 0x1c, 0x0d };

            var wrapperBuffer = new byte[body.Length + 3];
            Array.Copy(header, 0, wrapperBuffer, 0, 1);
            Array.Copy(body, 0, wrapperBuffer, 1, body.Length);
            Array.Copy(trailer, 0, wrapperBuffer, 1 + body.Length, 2);

            return wrapperBuffer;
        }

        private byte[] CheckAndExtractRecvHL7Message(byte[] data)
        {
            // ゼロデータを切り取り
            var zeroCount = data.Count(x => x == 0x00);
            var truncatedBuffer = new byte[data.Length - zeroCount];
            Array.Copy(data, truncatedBuffer, data.Length - zeroCount);

            // ヘッダ&トレーラチェック
            if (truncatedBuffer.Length > 4 &&
                (truncatedBuffer[0] != 0x0B || truncatedBuffer[^1] != 0x0D || truncatedBuffer[^2] != 0x1C))
            {
                return null;
            }

            var newBuffer = new byte[truncatedBuffer.Length - 3];
            Array.Copy(truncatedBuffer, 1, newBuffer, 0, truncatedBuffer.Length - 3);
            return newBuffer;
        }

        /// <summary>
        /// 2重起動禁止処理
        /// </summary>
        private static void PreventDuplicateInstances()
        {
            const string mutexName = "PHCVitalCollector";
            var mutex = new System.Threading.Mutex(true, mutexName, out var createdNew);

            //ミューテックスの初期所有権が付与されたか調べる
            if (!createdNew)
            {
                //されなかった場合は、すでに起動していると判断して終了
                mutex.Close();
                throw new InvalidOperationException("二重起動");
            }
        }

        public static bool IsExecCollectVital(TimeSpan baseTime, int execCount)
        {
            // 実行時間を計算
            var now = DateTime.Now;
            var baseHour = baseTime.Hours;
            var interval = 24 / execCount;

            for (var hour = baseHour; hour < 24; hour += interval)
            {
                if (now.Hour == hour && now.Minute <= 5 && now.Minute >= 55)
                {
                    return true;
                }
            }

            return false;
        }
    }

    public class VitalCollectorConfig
    {
        private const string SectionApp = "AppSettings";
        private const string SetSaveLogDirectory = "SaveLogDirectory";
        private const string SetSaveHL7Data = "SaveHL7Data";
        private const string SetDebugMode = "DebugMode";
        private const string SetGatewayAddress = "GatewayAddress";
        private const string SetGatewayPortNumber = "GatewayPortNumber";
        private const string SetGatewayTimeoutSec = "GatewayConnectTimeout";
        private const string SetECarteServerURL = "ECarteServer";
        private const string SetDataValidTimeSec = "DataValidTimeSec";
        private const string SetRegisterId = "RegisterId";
        private const string SetMaxPatientCount = "MaxPatientCount";
        private const string SetPreserveLogDays = "PreserveLogDays";

        private const string SaveLogDirectoryDefault = "log";
        private const bool SaveHL7Default = true;
        private const int GatewayTimeoutDefault = 5;
        private const int RegisterThresholdSecDefault = 3 * 60 * 60;
        private const int MaxPatientCountDefault = 50;
        private const int PreserveLogDaysDefault = 30;

        internal string SaveLogDirectory { get; set; } = SaveLogDirectoryDefault;
        internal bool SaveHL7 { get; set; } = SaveHL7Default;
        internal List<string> GatewayAddress { get; set; }
        internal List<int> GatewayPortNumber { get; set; }
        internal int GatewayConnectTimeout { get; set; } = GatewayTimeoutDefault;
        internal int RegisterThresholdSec { get; set; } = RegisterThresholdSecDefault;
        internal string LiveWorksUrl { get; set; }
        internal int RegisterId { get; set; } = 0;
        internal int MaxPatientCount { get; set; }
        internal int PreserveLogDays { get; set; }

        public static VitalCollectorConfig ReadConfiguration(IConfiguration config)
        {
            const string ConfigFilename = "AppConfig.json";
            var result = new VitalCollectorConfig();
            if (bool.TryParse(config[$"{SectionApp}:{SetSaveHL7Data}"], out var saveLog))
            {
                result.SaveHL7 = saveLog;
            }

            var saveLogDirectory = config[$"{SectionApp}:{SetSaveLogDirectory}"];
            if (!string.IsNullOrEmpty(saveLogDirectory))
            {
                result.SaveLogDirectory = saveLogDirectory;
                Logger.BaseDirectory = saveLogDirectory;
            }

            if (bool.TryParse(config[$"{SectionApp}:{SetDebugMode}"], out var debugMode))
            {
                Logger.DebugMode = debugMode;
            }

            if (!int.TryParse(config[$"{SectionApp}:{SetMaxPatientCount}"], out var maxPatientCount))
            {
                maxPatientCount = MaxPatientCountDefault;
            }

            if (!int.TryParse(config[$"{SectionApp}:{SetPreserveLogDays}"], out var preserveLogDays))
            {
                preserveLogDays = PreserveLogDaysDefault;
            }

            result.MaxPatientCount = maxPatientCount;
            result.PreserveLogDays = preserveLogDays;
            var gateways = config[$"{SectionApp}:{SetGatewayAddress}"]?.Split(",");
            var ports = config[$"{SectionApp}:{SetGatewayPortNumber}"]?.Split(",");

            if (gateways == null || ports == null)
            {
                throw new ArgumentException("ゲートウェイ設定が欠如しています。IPアドレスとポートの設定を確認してください");
            }

            var monitorCount = Math.Min(gateways.Length, ports.Length);

            result.GatewayAddress = new List<string>();
            result.GatewayPortNumber = new List<int>();

            for (var i = 0; i < monitorCount; i++)
            {
                if (!int.TryParse(ports[i], out var port))
                {
                    Logger.WriteLog($"ポート番号が読み込めません IP[{gateways[i]}] Port[{ports[i]}]");
                    continue;
                }
                result.GatewayAddress.Add(gateways[i]);
                result.GatewayPortNumber.Add(port);
            }
            result.LiveWorksUrl = config[$"{SectionApp}:{SetECarteServerURL}"];

            if (int.TryParse(config[$"{SectionApp}:{SetDataValidTimeSec}"], out var registerThresholdSec))
            {
                result.RegisterThresholdSec = registerThresholdSec;
            }

            if (int.TryParse(config[$"{SectionApp}:{SetGatewayTimeoutSec}"], out var gatewayTimeout))
            {
                result.GatewayConnectTimeout = gatewayTimeout;
            }

            if (int.TryParse(config[$"{SectionApp}:{SetRegisterId}"], out var registerId))
            {
                result.RegisterId = registerId;
            }

            Logger.WriteLog($@"設定ファイル[{Directory.GetCurrentDirectory()}\{ConfigFilename}]");
            return result;
        }
    }
}
