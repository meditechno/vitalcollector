﻿///////////////////////////////////////////////////////
// Copyright (c) 2020 PHC株式会社
// Filename: VitalCollectResponse.cs
///////////////////////////////////////////////////////
namespace VitalCollector.NihonKodenHL7
{
    using HL7.Dotnetcore;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    internal enum Status { None = 0, PID = 1, PV1 = 2, OBR = 3, OBX = 4 };

    /// <summary>
    /// プロトコル仕様書記載のサンプルデータを再現するようになっています。ORU^R01
    /// </summary>
    public class VitalCollectResponse
    {
        private const string MessageType = "ORU^R01^ORU_R01";
        private const string ProcessID = "P";
        private const string Version = "2.4";
        private const string ResponseType = "NE";
        private const string ApplicationResponseType = "AL";

        /// <summary>
        ///  候補の値リスト
        /// </summary>
        private static readonly List<OBXElement> OBXElements = new List<OBXElement>
        {
            new OBXElement { Subcode = "001000", Description = "VITAL HR",          Unit = "bpm"  },
            new OBXElement { Subcode = "002000", Description = "VITAL VPC",         Unit = "/min" },
            new OBXElement { Subcode = "003001", Description = "VITAL ST2",         Unit = "mV"   },
            new OBXElement { Subcode = "004001", Description = "VITAL APSEC(RESP)", Unit = "sec"  },
            new OBXElement { Subcode = "004005", Description = "VITAL rRESP(imp)",  Unit = "/min" },
            new OBXElement { Subcode = "005000", Description = "VITAL RESP",        Unit = "/min" },
            new OBXElement { Subcode = "007000", Description = "VITAL SpO2",        Unit = "%"    },
            new OBXElement { Subcode = "007001", Description = "VITAL PR(spo2)",    Unit = "/min" },
            new OBXElement { Subcode = "027000", Description = "VITAL Temperature", Unit = "C"    },
            new OBXElement { Subcode = "062000", Description = "VITAL PRESS(S)",    Unit = "mmHg" },
            new OBXElement { Subcode = "062001", Description = "VITAL PRESS(D)",    Unit = "mmHg" },
            new OBXElement { Subcode = "062002", Description = "VITAL PRESS(M)",    Unit = "mmHg" },
            new OBXElement { Subcode = "062003", Description = "VITAL PR(p1)",      Unit = "/min" },
            new OBXElement { Subcode = "009000", Description = "NIBP SYS",          Unit = "mmHg" },
            new OBXElement { Subcode = "009001", Description = "NIBP DIAS",         Unit = "mmHg" },
            new OBXElement { Subcode = "009002", Description = "NIBP MEAN",         Unit = "mmHg" }
        };

        public Dictionary<string, Person> Results { get; set; } = new Dictionary<string, Person>();
        public string MessageControlID { get; set; }

        public Message CreateAckMessage()
        {
            var message = new Message();

            // MSH
            const string alldelimeter = @"|^~\&";
            var todate = DateTime.Now.ToString("yyyyMMddHHmmss");
            var msgstring = $"MSH{alldelimeter}|HL7Gateway|Hospital|VitalCollector|Hospital|{todate}||ACK^R01^ACK|{todate}|P|2.4|||NE|AL||ASCII||ASCII\r";
            var mshmessage = new Message(msgstring);
            mshmessage.ParseMessage();
            message.AddNewSegment(mshmessage.DefaultSegment("MSH"));

            // MSA
            var msa = new Segment("MSA", new HL7Encoding());
            msa.AddNewField("AA");
            msa.AddNewField(MessageControlID);
            msa.AddEmptyField();
            msa.AddEmptyField();
            msa.AddEmptyField();
            msa.AddEmptyField();
            message.AddNewSegment(msa);

            return message;
        }

        public void ParseMessage(Message message)
        {
            this.ValidateResponseMessage(message);

            var errorMessage = string.Empty;
            var status = Status.None;
            Person personWork = null;
            OBRElement obrWork = null;

            foreach (var segment in message.Segments())
            {
                switch (status)
                {
                    case Status.None:
                        if (segment.Name == "MSH")
                        {
                            MessageControlID = segment.Fields(10).Value;
                            status = Status.PID;
                        }
                        break;
                    case Status.PID:
                        if (segment.Name == "PID")
                        {
                            var pid = segment.Fields(3).Value;
                            if (!int.TryParse(pid, out _))
                            {
                                errorMessage += $"患者番号{pid}が不正です{Environment.NewLine}";
                                continue;
                            }

                            if (!Results.TryGetValue(pid, out var patient))
                            {
                                patient = new Person
                                {
                                    PatientID = pid,
                                    OBRElements = new List<OBRElement>()
                                };
                                personWork = patient;
                                Results.Add(pid, patient);
                            }

                            status = Status.PV1;
                        }
                        break;
                    case Status.PV1:
                        if (segment.Name == "PV1")
                        {
                            var bedInformation = segment.Fields(3)?.Value;
                            if (personWork != null)
                            {
                                personWork.PV1Element.BedInformation = bedInformation;
                            }
                            status = Status.OBR;
                        }
                        break;
                    case Status.OBR:
                        if (segment.Name == "OBR")
                        {
                            if (personWork == null)
                            {
                                continue;
                            }

                            obrWork = new OBRElement
                            {
                                Description = segment.Fields(4).Value,
                                ExDatetime = segment.Fields(7).Value,
                                OBXElements = new List<OBXElement>()
                            };
                            personWork.OBRElements.Add(obrWork);
                            status = Status.OBX;
                        }
                        break;
                    case Status.OBX:
                        if (segment.Name == "OBX")
                        {
                            if (personWork == null || obrWork == null)
                            {
                                continue;
                            }

                            var key = segment.Fields(3)?.Components(1)?.Value;
                            // コピーしないと値が上書きされる
                            if (OBXElements.SingleOrDefault(x => x.Subcode == key)?.Clone() is OBXElement element)
                            {
                                element.ObservedDate = segment.Fields(14).Value;
                                element.Value = segment.Fields(5).Value;
                                obrWork.OBXElements.Add(element);
                            }
                        }
                        if (segment.Name == "OBR")
                        {
                            if (personWork == null)
                            {
                                continue;
                            }

                            obrWork = new OBRElement
                            {
                                Description = segment.Fields(4).Value,
                                ExDatetime = segment.Fields(7).Value,
                                OBXElements = new List<OBXElement>()
                            };
                            personWork.OBRElements.Add(obrWork);
                        }
                        if (segment.Name == "PID")
                        {
                            var pid = segment.Fields(3).Value;
                            if (!int.TryParse(pid, out _))
                            {
                                errorMessage += $"患者番号{pid}が不正です{Environment.NewLine}";
                                continue;
                            }

                            if (!Results.TryGetValue(pid, out var patient))
                            {
                                patient = new Person
                                {
                                    PatientID = pid,
                                    OBRElements = new List<OBRElement>()
                                };
                                personWork = patient;
                                Results.Add(pid, patient);
                            }

                            status = Status.OBR;
                        }
                        break;
                }
            }

            if (!string.IsNullOrEmpty(errorMessage))
            {
                throw new ArgumentException(errorMessage);
            }
        }

        private void ValidateResponseMessage(Message message)
        {
            var messageType = message.GetValue("MSH.9");
            if (messageType != MessageType)
            {
                throw new ArgumentException($"MSHメッセージタイプが不正です[{messageType}]");
            }

            var processID = message.GetValue("MSH.11");
            if (processID != ProcessID)
            {
                throw new ArgumentException($"MSH処理IDが不正です[{processID}]");
            }

            var version = message.GetValue("MSH.12");
            if (version != Version)
            {
                throw new ArgumentException($"MSHバージョンが不正です[{version}]");
            }

            var responseType = message.GetValue("MSH.15");
            if (responseType != ResponseType)
            {
                throw new ArgumentException($"MSH受諾応答型が不正です[{responseType}]");
            }

            var applicationResponseType = message.GetValue("MSH.16");
            if (applicationResponseType != ApplicationResponseType)
            {
                throw new ArgumentException($"MSHアプリケーション応答型が不正です[{applicationResponseType}]");
            }

            //var positiveResponse = message.GetValue("MSA.1");
            //if (positiveResponse != PositiveResponseCodeA && positiveResponse != PositiveResponseCodeC)
            //{
            //    throw new ArgumentException($"MSA肯定応答コードが不正です[{positiveResponse}]");
            //}
        }
    }

    public class Person
    {
        internal PV1Element PV1Element = new PV1Element();

        internal List<OBRElement> OBRElements = new List<OBRElement>
        {
            new OBRElement
            {
                Description = "VITAL",
                OBXElements = new List<OBXElement>
                {
                    // 呼吸数
                    new OBXElement { Subcode = "005000", Description = "VITAL RESP",        Unit = "/min" },
                    // 酸素飽和度
                    new OBXElement { Subcode = "007000", Description = "VITAL SpO2",        Unit = "%"    },
                    // 脈拍
                    new OBXElement { Subcode = "007001", Description = "VITAL PR(spo2)",    Unit = "/min" },
                    // 最高血圧
                    new OBXElement { Subcode = "009000", Description = "NIBP SYS",          Unit = "mmHg" },
                    // 最低血圧
                    new OBXElement { Subcode = "009001", Description = "NIBP DIAS",         Unit = "mmHg" },
                    // 体温
                    new OBXElement { Subcode = "027000", Description = "VITAL Temperature", Unit = "C"    },
                }
            }
        };

        // 一通りVITALデータが揃っているかで判断する
        public bool? HasData => OBRElements[0].OBXElements?.All(x => x.Value != null);

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PatientID { get; set; }
        public string Gender { get; set; }
        public string BirthDate { get; set; }
    }

    /// <summary>
    /// HL7要素 - PV1
    /// </summary>
    internal class PV1Element
    {
        public string BedInformation { get; set; }
    }

    /// <summary>
    /// HL7要素 - OBR
    /// </summary>
    internal class OBRElement
    {
        public string Description { get; set; }
        public string ExDatetime { get; set; }
        public List<OBXElement> OBXElements { get; set; }
    }

    /// <summary>
    /// HL7要素 - OBX
    /// </summary>
    internal class OBXElement : ICloneable
    {
        public string Subcode { get; set; }
        public string Description { get; set; }
        public string SubID { get; set; } = "1";
        public string Value { get; set; }
        public string Unit { get; set; }
        public string Final { get; set; } = "F";
        public string ObservedDate { get; set; }

        public object Clone() => MemberwiseClone();
    }
}
