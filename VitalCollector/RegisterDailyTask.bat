@echo off
rem echo on

@if "%1"=="" (
	set starttime=00:00
) else (
	set starttime=%1
)
@if "%2"=="" (
	set execcount=6
) else (
	set execcount=%2
)

if "%3"=="" (
	set interval=20
) else (
	set interval=%3
)

set flag=FALSE
if %execcount%==1 set flag=TRUE
if %execcount%==2 set flag=TRUE
if %execcount%==3 set flag=TRUE
if %execcount%==4 set flag=TRUE
if %execcount%==6 set flag=TRUE
if %execcount%==8 set flag=TRUE
if %execcount%==12 set flag=TRUE
if %execcount%==24 set flag=TRUE

if %flag%==FALSE (
	rem 引数が無効
	echo 1日の実行回数は２４から割り切れる数値である必要があります。初期値の６回を設定します
	set execcount=6
)

echo 初回実行時間=%starttime%
echo 1日の実行回数=%execcount%

schtasks /create /tn VitalCollector /tr "%~dp0ExecVitalCollector.bat %starttime% %execcount%" /sc minute /mo %interval% /st %starttime% /rl highest /F
pause
