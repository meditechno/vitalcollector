﻿///////////////////////////////////////////////////////
// Copyright (c) 2020 PHC株式会社
// Filename: BloodPressureManager.cs
///////////////////////////////////////////////////////

namespace VitalCollector.Data
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Runtime.Serialization;
    using System.Runtime.Serialization.Json;
    using System.Text.Json;

    public static class BloodPressureManager
    {
        private const string BPDataFile = "LatestBloodPressure.dat";
        private static Dictionary<string, BloodPressureData> BloodPressureMap = new Dictionary<string, BloodPressureData>();

        public static void SavePersonData(string patientID, string systolic, string diastolic, string datetime)
        {
            if (!int.TryParse(systolic, out var systolicValue) || !int.TryParse(diastolic, out var diastolicValue))
            {
                throw new InvalidOperationException();
            }

            if (!BloodPressureMap.TryGetValue(patientID, out var data))
            {
                data = new BloodPressureData();
                BloodPressureMap.Add(patientID, data);
            }

            data.PatientId = patientID;
            data.Systolic = systolicValue;
            data.Diastolic = diastolicValue;
            data.LastMeasurementDateTime = datetime;
        }

        public static BloodPressureData GetBloodPressureData(string patientID)
        {
            BloodPressureMap.TryGetValue(patientID, out var data);
            return data;
        }

        public static void SaveData()
        {
            var options = new JsonSerializerOptions
            {
                WriteIndented = true
            };

            var jsondata = JsonSerializer.Serialize(BloodPressureMap, typeof(Dictionary<string, BloodPressureData>), options);
            File.WriteAllText(BPDataFile, jsondata);
        }

        public static void LoadData()
        {
            if (!File.Exists(BPDataFile))
            {
                SaveData();
            }

            var jsondata = File.ReadAllText(BPDataFile);
            BloodPressureMap = JsonSerializer.Deserialize<Dictionary<string, BloodPressureData>>(jsondata);
        }
    }
}
