﻿///////////////////////////////////////////////////////
// Copyright (c) 2020 PHC株式会社
// Filename: BloodPressureData.cs
///////////////////////////////////////////////////////

namespace VitalCollector.Data
{
    public class BloodPressureData
    {
        /// <summary>
        /// 患者id
        /// </summary>
        public string PatientId { get; set; }

        /// <summary>
        /// 前回計測日時
        /// </summary>
        public string LastMeasurementDateTime { get; set; }

        /// <summary>
        /// 最高血圧
        /// </summary>
        public int Systolic { get; set; }

        /// <summary>
        /// 最低血圧
        /// </summary>
        public int Diastolic { get; set; }
    }
}
