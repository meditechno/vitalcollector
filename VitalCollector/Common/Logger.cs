﻿///////////////////////////////////////////////////////
// Copyright (c) 2020 PHC株式会社
// Filename: Logger.cs
///////////////////////////////////////////////////////
namespace VitalCollector.Common
{
    using System;
    using System.Globalization;
    using System.IO;
    using System.Text.RegularExpressions;

    /// <summary>
    /// ロガークラス
    /// </summary>
    public static class Logger
    {
        public enum Mode { GWRecv, GWSend, LWRecv, LWSend };
        private const string LogFileName = "VitalCollector.log";
        private const string TempLogFileName = "VitalCollectortemp.log";
        public static bool DebugMode { get; set; } = false;
        public static string BaseDirectory { get; set; }

        /// <summary>
        /// ログ記録の追加
        /// </summary>
        /// <param name="message">ログメッセージ</param>
        /// <param name="filename">ファイル名</param>
        /// <param name="e">例外オブジェクト</param>
        public static void WriteLog(string message, string filename = null, Exception e = null)
        {
            if (!string.IsNullOrEmpty(BaseDirectory) && !Directory.Exists(BaseDirectory))
            {
                Directory.CreateDirectory(BaseDirectory);
            }

            using var writer = new StreamWriter(Path.Combine(BaseDirectory ?? string.Empty, LogFileName), true);
            var now = DateTime.Now;
            // デバッグモードによってスタックトレースを出力するかを判断
            var ex = !DebugMode || e == null ? string.Empty : e.StackTrace;
            var fileInfo = string.IsNullOrEmpty(filename) ? string.Empty : $"ファイル[{filename}]";
            var detail = $"{now}: {fileInfo} {message} {ex}";
            writer.WriteLine(detail);
            Console.WriteLine(detail);
        }

        /// <summary>
        /// 送受信バイナリデータをファイルに落とし込む
        /// </summary>
        /// <param name="data">送受信データ</param>
        /// <param name="mode">送信か受信か</param>
        public static void WriteBinaryToLogfile(byte[] data, int size, Mode mode)
        {
            var writebytes = new byte[size];
            Array.Copy(data, writebytes, size);
            var today = DateTime.Now.ToString("yyyyMMddHHmmssfff");
            var filename = $"{mode}{today}.log";

            if (!string.IsNullOrEmpty(BaseDirectory) && !Directory.Exists(BaseDirectory))
            {
                Directory.CreateDirectory(BaseDirectory);
            }

            File.WriteAllBytes(Path.Combine(BaseDirectory ?? string.Empty, filename), writebytes);
        }

        /// <summary>
        /// 送受信データをファイルに落とし込む
        /// </summary>
        /// <param name="data">送受信データ</param>
        /// <param name="mode">送信か受信か</param>
        public static void WriteTextToLogfile(string data, Mode mode)
        {
            var today = DateTime.Now.ToString("yyyyMMddHHmmssfff");
            var filename = $"{mode}{today}.log";

            if (!string.IsNullOrEmpty(BaseDirectory) && !Directory.Exists(BaseDirectory))
            {
                Directory.CreateDirectory(BaseDirectory);
            }

            File.WriteAllText(Path.Combine(BaseDirectory ?? string.Empty, filename), data);
        }

        /// <summary>
        /// メインログファイルのローテート
        /// </summary>
        public static void RotateMainLog(VitalCollectorConfig config)
        {
            if (!string.IsNullOrEmpty(BaseDirectory) && !Directory.Exists(BaseDirectory))
            {
                // ディレクトリがないのでクリーンしない
                return;
            }

            using var writer = new StreamWriter(Path.Combine(BaseDirectory ?? string.Empty, TempLogFileName), true);
            using var reader = new StreamReader(Path.Combine(BaseDirectory ?? string.Empty, LogFileName));

            // 設定日数
            var thresholdDate = DateTime.Today.AddDays(config.PreserveLogDays * -1);

            while (reader.Peek() != -1)
            {
                var line = reader.ReadLine();
                var match = Regex.Match(line, @"^\d\d\d\d/\d\d/\d\d");
                if (DateTime.TryParse(match.Value, out var logdate))
                {
                    // 保持範囲であればキープ
                    var comp = logdate.CompareTo(thresholdDate);
                    if (comp >= 0)
                    {
                        writer.WriteLine(line);
                    }
                }
            }

            reader.Close();
            writer.Close();

            // ファイルコピー
            File.Delete(Path.Combine(BaseDirectory ?? string.Empty, LogFileName));
            File.Copy(Path.Combine(BaseDirectory ?? string.Empty, TempLogFileName), Path.Combine(BaseDirectory ?? string.Empty, LogFileName));
            File.Delete(Path.Combine(BaseDirectory ?? string.Empty, TempLogFileName));
            WriteLog("ログローテート完了");
        }

        /// <summary>
        /// 通信ログファイルのローテート（古いものの削除）
        /// </summary>
        public static void RotateFileLog(VitalCollectorConfig config)
        {
            var deleteCount = 0;
            // 設定日数
            var thresholdDate = DateTime.Today.AddDays(config.PreserveLogDays * -1);

            var files = Directory.GetFiles(BaseDirectory, "*.log", SearchOption.AllDirectories);
            foreach (var file in files)
            {
                // GWRecv
                var matchGWRecv = Regex.Match(file, @"GWRecv\d\d\d\d\d\d\d\d");
                var dategwrecv = matchGWRecv.Value.Replace("GWRecv", string.Empty);
                if (DateTime.TryParseExact(dategwrecv, "yyyyMMdd", null, DateTimeStyles.None, out var gwrecv))
                {
                    var comp = gwrecv.CompareTo(thresholdDate);
                    if (comp < 0)
                    {
                        File.Delete(file);
                        deleteCount++;
                    }
                }

                // GWSend
                var matchGWSend = Regex.Match(file, @"GWSend\d\d\d\d\d\d\d\d");
                var dategwsend = matchGWSend.Value.Replace("GWSend", string.Empty);
                if (DateTime.TryParseExact(dategwsend, "yyyyMMdd", null, DateTimeStyles.None, out var gwsend))
                {
                    var comp = gwsend.CompareTo(thresholdDate);
                    if (comp < 0)
                    {
                        File.Delete(file);
                        deleteCount++;
                    }
                }

                // LWSend
                var matchLWSend = Regex.Match(file, @"LWSend\d\d\d\d\d\d\d\d");
                var datelwsend = matchLWSend.Value.Replace("LWSend", string.Empty);
                if (DateTime.TryParseExact(datelwsend, "yyyyMMdd", null, DateTimeStyles.None, out var lwsend))
                {
                    var comp = lwsend.CompareTo(thresholdDate);
                    if (comp < 0)
                    {
                        File.Delete(file);
                        deleteCount++;
                    }
                }
            }

            if (deleteCount > 0)
            {
                Logger.WriteLog($"通信ログを{deleteCount}件削除しました");
            }
        }
    }
}
