﻿///////////////////////////////////////////////////////
// Copyright (c) 2020 PHC株式会社
// Filename: RegisterLiveWorks.cs
///////////////////////////////////////////////////////

namespace VitalCollector
{
    using global::VitalCollector.Common;
    using global::VitalCollector.Data;
    using global::VitalCollector.NihonKodenHL7;
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Net;
    using System.Net.Http;
    using System.Threading.Tasks;

    public static class RegisterLiveWorks
    {
        public static VitalCollectorConfig Config { get; set; }
        private const string Shori_kubun = "1";
        private const string SystolicCode = "009000";
        private const string DiastolicCode = "009001";
        private const string TemperatureCode = "027000";
        private const string PulseCode = "007001";
        private const string RespRateCode = "005000";
        private const string SPO2Code = "007000";

        public static async Task<int> RegisterBloodPressureAsync(string serverurl, List<Person> resultList)
        {
            var successCount = 0;
            // 患者単位で登録
            foreach (var person in resultList)
            {
                var obxElements = person.OBRElements.SingleOrDefault(x => x.Description == "NIBP")?.OBXElements;

                // 計測日と時間は個別の項目から取る（最高血圧に登録がなければ最低血圧を利用。なければエラー）
                var observedDateStr = obxElements?.SingleOrDefault(x => x.Subcode == SystolicCode)?.ObservedDate;
                if (string.IsNullOrEmpty(observedDateStr) ||
                    !DateTime.TryParseExact(observedDateStr, "yyyyMMddHHmmss", CultureInfo.InvariantCulture, DateTimeStyles.None, out var obxDateTime))
                {
                    observedDateStr = obxElements?.SingleOrDefault(x => x.Subcode == DiastolicCode)?.ObservedDate;
                    if (string.IsNullOrEmpty(observedDateStr) ||
                        !DateTime.TryParseExact(observedDateStr, "yyyyMMddHHmmss", CultureInfo.InvariantCulture, DateTimeStyles.None, out obxDateTime))
                    {
                        // 日時データ取得できずエラー
                        Logger.WriteLog($"データから日時データが取得できないため，登録しません PID[{person.PatientID}]");
                        continue;
                    }
                }

                var systolic = obxElements?.SingleOrDefault(x => x.Subcode == SystolicCode)?.Value;  // NiBP        - Systolic (mmHg)          009000 -> SYS||127|mmHg
                var diastolic = obxElements?.SingleOrDefault(x => x.Subcode == DiastolicCode)?.Value;  // NiBP        - Diastolic (mmHg)        009001 -> DIAS||127|mmHg

                // 血圧に関しては有効期限時間は考慮しないが，前回送信データとタイムスタンプが一緒であれば登録しない
                var lastData = BloodPressureManager.GetBloodPressureData(person.PatientID);
                if (lastData?.LastMeasurementDateTime == observedDateStr)
                {
                    Logger.WriteLog($"送信済のデータのため，登録しません PID[{person.PatientID}] 計測時間[{observedDateStr}] 最高血圧[{systolic ?? "なし"}]mmHg 最低血圧[{diastolic ?? "なし"}]mmHg");
                    continue;
                }

                var parameters = PrepareParameterBase(person.PatientID, observedDateStr);
                var logMessage = PrepareLogMessageBase(person.PatientID, observedDateStr);

                if (systolic != null)
                {
                    parameters.Add("最高血圧", systolic);
                    logMessage += $"最高血圧[{systolic}]" + Environment.NewLine;
                }
                else
                {
                    logMessage += "最高血圧[なし]" + Environment.NewLine;
                }

                if (diastolic != null)
                {
                    parameters.Add("最低血圧", diastolic);
                    logMessage += $"最低血圧[{diastolic}]" + Environment.NewLine;
                }
                else
                {
                    logMessage += "最低血圧[なし]" + Environment.NewLine;
                }

                // 送信
                if (await SendRegisterationAsync(person.PatientID, serverurl, logMessage, parameters).ConfigureAwait(false))
                {
                    successCount++;

                    //成功したらデータ登録
                    BloodPressureManager.SavePersonData(person.PatientID, systolic, diastolic, observedDateStr);
                }
            }
            return successCount;
        }

        public static async Task<int> RegisterVitalInfoAsync(string serverurl, long timeoutsec, List<Person> resultList)
        {
            var successCount = 0;
            // 患者単位で登録
            foreach (var person in resultList)
            {
                // 計測日と時間はOBRから取得する
                var vitalObrTime = person.OBRElements.SingleOrDefault(x => x.Description.StartsWith("VITAL"))?.ExDatetime;
                if (string.IsNullOrEmpty(vitalObrTime) || !DateTime.TryParseExact(vitalObrTime, "yyyyMMddHHmmss", CultureInfo.InvariantCulture, DateTimeStyles.None, out var obrDateTime))
                {
                    continue;
                }

                var diff = DateTime.Now.Subtract(obrDateTime).TotalSeconds;
                if (diff > timeoutsec)
                {
                    // 有効期限範囲外なので登録しない
                    Logger.WriteLog($"有効期限範囲外のデータのため，登録しません PID[{person.PatientID}] 計測時間[{obrDateTime}] 時間差[{diff}]秒");
                    continue;
                }

                var obxElements = person.OBRElements.SingleOrDefault(x => x.Description == "VITAL")?.OBXElements;
                var parameters = PrepareParameterBase(person.PatientID, vitalObrTime);
                var logMessage = PrepareLogMessageBase(person.PatientID, vitalObrTime);

                var temperature = obxElements?.SingleOrDefault(x => x.Subcode == TemperatureCode)?.Value;  // Temp        - Temperature (C)          027000 -> TEMP||37.1|C
                if (temperature != null)
                {
                    parameters.Add("体温", temperature);
                    logMessage += $"体温[{temperature}]" + Environment.NewLine;
                }
                else
                {
                    logMessage += "体温[なし]" + Environment.NewLine;
                }

                var pulse = obxElements?.SingleOrDefault(x => x.Subcode == PulseCode)?.Value;        // SpO2        - Average PR (/min)        007001 -> PR(spo2)||82|/min
                if (pulse != null)
                {
                    parameters.Add("脈拍", pulse);
                    logMessage += $"脈拍[{pulse}]" + Environment.NewLine;
                }
                else
                {
                    logMessage += "脈拍[なし]" + Environment.NewLine;
                }

                var respiration = obxElements?.SingleOrDefault(x => x.Subcode == RespRateCode)?.Value;  // Impedance Respiration - Impedance Resp Rate (/min) 005000 -> RESP||12|/min
                if (respiration != null)
                {
                    parameters.Add("呼吸数", respiration);
                    logMessage += $"呼吸数[{respiration}]" + Environment.NewLine;
                }
                else
                {
                    logMessage += "呼吸数[なし]" + Environment.NewLine;
                }

                var saturation = obxElements?.SingleOrDefault(x => x.Subcode == SPO2Code)?.Value;   // SpO2        - SpO2 (%)                        -> SpO2||98|%
                if (saturation != null)
                {
                    parameters.Add("酸素飽和度", saturation);
                    logMessage += $"酸素飽和度[{saturation}]" + Environment.NewLine;
                }
                else
                {
                    logMessage += "酸素飽和度[なし]" + Environment.NewLine;
                }

                logMessage += $@"記事記載区分[{0}]
登録者id[{Config.RegisterId}]";

                if (await SendRegisterationAsync(person.PatientID, serverurl, logMessage, parameters).ConfigureAwait(false))
                {
                    successCount++;
                }
            }
            return successCount;
        }

        private static string PrepareLogMessageBase(string patientID, string datetime) =>
$@"LiveWorks登録開始
プロジェクトcd[L01A01_002020_01]
処理区分[{Shori_kubun}]
PID[{patientID}]
計測時間[{datetime}]
";

        private static Dictionary<string, string> PrepareParameterBase(string patientID, string datetime) =>
            new Dictionary<string, string>
                {
                    { "プロジェクトcd", "L01A01_002020_01" },
                    { "処理区分", Shori_kubun }, // 新規・更新
                    { "患者番号", patientID },
                    { "計測日", datetime.Substring(0, 8) },
                    { "計測時間", datetime.Substring(8, 6) },
                    { "記事記載区分", "0" }, //
                    { "登録者id", $"{Config.RegisterId}" }, //
                };

        private static async Task<bool> SendRegisterationAsync(string patientID, string serverurl, string logMessage, Dictionary<string, string> parameters)
        {
            var success = false;
            var content = new FormUrlEncodedContent(parameters);
            try
            {
                using var httpClient = new HttpClient();
                var httpresponse = await httpClient.PostAsync(serverurl, content).ConfigureAwait(false);
                if (httpresponse.StatusCode == HttpStatusCode.OK)
                {
                    Logger.WriteLog($"登録成功 患者id[{patientID}] HttpCode[{httpresponse.StatusCode}] Header[{httpresponse.Headers}] Content[{httpresponse.Content}]");
                    success = true;
                }
                else
                {
                    var errMsg = await httpresponse.Content.ReadAsStringAsync().ConfigureAwait(false);
                    Logger.WriteLog($"登録失敗 患者id[{patientID}] コード[{errMsg}] HttpCode[{httpresponse.StatusCode}] Header[{httpresponse.Headers}] Content[{httpresponse.Content}]");
                }
            }
            catch (Exception e)
            {
                Logger.WriteLog($"登録失敗 患者id[{patientID}] 詳細[{e.Message}]");
                throw;
            }
            finally
            {
                if (Config.SaveHL7)
                {
                    Logger.WriteTextToLogfile(logMessage, Logger.Mode.LWSend);
                }
            }
            return success;
        }
    }
}
