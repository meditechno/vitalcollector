﻿///////////////////////////////////////////////////////
// Copyright (c) 2020 PHC株式会社
// Filename: Program.cs
///////////////////////////////////////////////////////

namespace VitalCollector
{
    using Microsoft.Extensions.Configuration;
    using System;
    using System.IO;
    using System.Threading.Tasks;

    public static class Program
    {
        private const string ConfigFilename = "AppConfig.json";

        private static async Task Main(string[] args)
        {
            if (args is null)
            {
                throw new ArgumentNullException(nameof(args));
            }

            if (args.Length != 2)
            {
                throw new ArgumentException("引数を２つ指定してください： VitalCollector.exe [基底時間] [一日の起動回数]");
            }

            if (!TimeSpan.TryParse(args[0], out var baseTime))
            {
                throw new ArgumentException($"引数 [基底時間] が無効です: {args[0]}");
            }

            if (!int.TryParse(args[1], out var count))
            {
                throw new ArgumentException($"引数 [一日の起動回数] が無効です: {args[1]}");
            }

            var execVitalCollector = VitalCollector.IsExecCollectVital(baseTime, count);
            var collector = new VitalCollector
            {
                Configuration = GetConfiguration()
            };
            await collector.ExecuteAsync(execVitalCollector).ConfigureAwait(false);
        }

        private static IConfiguration GetConfiguration()
        {
            var configBuilder = new ConfigurationBuilder();

            // 設定ファイルのベースパスをカレントディレクトリ( 実行ファイルと同じディレクトリ )にします。
            configBuilder.SetBasePath(Directory.GetCurrentDirectory());

            // Json ファイルへのパスを設定します。SetBasePath() で設定したパスからの相対パスになります。
            configBuilder.AddJsonFile(ConfigFilename);

            return configBuilder.Build();
        }
    }
}
