﻿///////////////////////////////////////////////////////
// Copyright (c) 2019 PHC株式会社
// Filename: PatientInfoClientTest.cs
///////////////////////////////////////////////////////

namespace PatientInfoProviderTest
{
    using HL7.Dotnetcore;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using PatientInfoProvider.Data;
    using PatientInfoProviderTest.Common;
    using PatientInfoProviderTest.NihonKodenHL7;
    using Shouldly;
    using System;
    using System.IO;
    using System.Linq;
    using System.Net.Sockets;
    using System.Text;
    using System.Threading;

    [TestClass]
    public class PatientInfoClientTest
    {
        private const string server = "localhost";
        private const int port = 12345;

        private const int PatientNumber = 99900050;
        private const string ZPatientNumber = "９９９０００５０";
        private const int GenderCode = 2; // 女性
        private const string KanjiLastName = "入院患者テスト";
        private const string KanjiFirstName = "";
        private const string KanaLastName = "にゅういんてすと";
        private const string KanaFirstName = "";
        private const string BloodType = "AB+"; // 血液型
        private const string BirthDate = "19900401";
        private const string FirstTime = "210000";
        private const string LastTime = "235959";
        private const double FirstHeight = 162.1;
        private const double LastHeight = 162.2;
        private const double YesterdayHeight = 162.0;
        private const double FirstWeight = 62.1;
        private const double LastWeight = 62.2;
        private const double YesterdayWeight = 62.0;

        static PatientInfoClientTest()
        {
            DBSetting dbsetting = new DBSetting
            {
                //Address = "192.168.100.103",
                //PortNumber = "5432",
                //DatabaseName = "live",
                //Encoding = "UTF8",
                //Username = "liveadm",
                //Password = "liveadm"
                Address = "localhost",
                PortNumber = "5432",
                DatabaseName = "PostgreSQL",
                Encoding = "UTF8",
                Username = "postgres",
                Password = "pass"
            };
            var _ = new DB(dbsetting);
        }

        [TestInitialize]
        public void TestSetupDatabase()
        {
            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
            SetupPatientInfo();
            SetupHeightHistory();
            SetupWeightHistory();
        }

        private void SetupBloodTypeEmpty()
        {
            var info = BasicPatientInfo.GetPatientInfo(PatientNumber).FirstOrDefault();
            info.血液型cd = PatientInfoProvider.Data.BloodType.GetBloodType(string.Empty);
            info.Update();
        }

        private void SetupPatientInfo()
        {
            var info = BasicPatientInfo.GetPatientInfo(PatientNumber).FirstOrDefault();
            if (info == null)
            {
                info = new BasicPatientInfo
                {
                    患者id = PatientNumber,
                    患者番号 = PatientNumber
                };
                info.Insert();
            }

            if (info.かな氏名 != $"{KanaLastName} {KanaFirstName}")
            {
                info.かな氏名 = $"{KanaLastName} {KanaFirstName}";
            }

            if (info.漢字氏名 != $"{KanjiLastName} {KanjiFirstName}")
            {
                info.漢字氏名 = $"{KanjiLastName} {KanjiFirstName}";
            }

            if (info.性別 != GenderCode)
            {
                info.性別 = GenderCode;
            }

            if (PatientInfoProvider.Data.BloodType.GetBloodTypeById(info.血液型cd) != BloodType)
            {
                info.血液型cd = PatientInfoProvider.Data.BloodType.GetBloodType(BloodType);
            }

            if (info.生年月日 != BirthDate)
            {
                info.生年月日 = BirthDate;
            }

            info.Update();
        }

        private void SetupHeightEmpty()
        {
            HeightHistory.DeleteAll(PatientNumber);
        }

        private void SetupHeightHistory()
        {
            // 昨日と今日のデータを作成
            var yesterdate = DateTime.Today.AddDays(-1).ToString("yyyyMMdd");
            var yesterday = HeightHistory.GetHeightDatas(PatientNumber, yesterdate).OrderByDescending(x => x.計測時間).FirstOrDefault();
            if (yesterday == null)
            {
                var sequenceNumber = HeightHistory.GetMaxSequenceNumber() + 1;
                yesterday = new HeightHistory
                {
                    患者id = PatientNumber,
                    患者身長歴連番号 = sequenceNumber,
                    計測日 = yesterdate
                };
                yesterday.Insert();
            }

            if (yesterday.計測時間 != LastTime)
            {
                yesterday.計測時間 = LastTime;
            }

            if (yesterday.身長 != YesterdayHeight)
            {
                yesterday.身長 = YesterdayHeight;
            }

            yesterday.Update();

            var todate = DateTime.Today.ToString("yyyyMMdd");
            var todayList = HeightHistory.GetHeightDatas(PatientNumber, todate).OrderByDescending(x => x.計測時間);
            var todayLast = todayList.FirstOrDefault();
            if (todayLast == null)
            {
                var sequenceNumber = HeightHistory.GetMaxSequenceNumber() + 1;
                todayLast = new HeightHistory
                {
                    患者id = PatientNumber,
                    患者身長歴連番号 = sequenceNumber,
                    計測日 = todate
                };
                todayLast.Insert();
            }

            if (todayLast.計測時間 != LastTime)
            {
                todayLast.計測時間 = LastTime;
            }

            if (todayLast.身長 != LastHeight)
            {
                todayLast.身長 = LastHeight;
            }

            todayLast.Update();

            var todayFirst = todayList.Count() > 1 ? todayList.ToList()[1] : null;
            if (todayFirst == null)
            {
                var sequenceNumber = HeightHistory.GetMaxSequenceNumber() + 1;
                todayFirst = new HeightHistory
                {
                    患者id = PatientNumber,
                    患者身長歴連番号 = sequenceNumber,
                    計測日 = todate
                };
                todayFirst.Insert();
            }

            if (todayFirst.計測時間 != FirstTime)
            {
                todayFirst.計測時間 = FirstTime;
            }

            if (todayFirst.身長 != FirstHeight)
            {
                todayFirst.身長 = FirstHeight;
            }

            todayFirst.Update();
        }

        private void SetupWeightEmpty()
        {
            WeightHistory.DeleteAll(PatientNumber);
        }

        private void SetupWeightHistory()
        {
            // 昨日と今日のデータを作成
            var yesterdate = DateTime.Today.AddDays(-1).ToString("yyyyMMdd");
            var yesterday = WeightHistory.GetWeightDatas(PatientNumber, yesterdate).OrderByDescending(x => x.計測時間).FirstOrDefault();
            if (yesterday == null)
            {
                var sequenceNumber = WeightHistory.GetMaxSequenceNumber() + 1;
                yesterday = new WeightHistory
                {
                    患者id = PatientNumber,
                    患者体重歴連番号 = sequenceNumber,
                    計測日 = yesterdate
                };
                yesterday.Insert();
            }

            if (yesterday.計測時間 != LastTime)
            {
                yesterday.計測時間 = LastTime;
            }

            if (yesterday.体重 != YesterdayWeight)
            {
                yesterday.体重 = YesterdayWeight;
            }

            yesterday.Update();

            var todate = DateTime.Today.ToString("yyyyMMdd");
            var todayList = WeightHistory.GetWeightDatas(PatientNumber, todate).OrderByDescending(x => x.計測時間);
            var todayLast = todayList.FirstOrDefault();
            if (todayLast == null)
            {
                var sequenceNumber = WeightHistory.GetMaxSequenceNumber() + 1;
                todayLast = new WeightHistory
                {
                    患者id = PatientNumber,
                    患者体重歴連番号 = sequenceNumber,
                    計測日 = todate
                };
                todayLast.Insert();
            }

            if (todayLast.計測時間 != LastTime)
            {
                todayLast.計測時間 = LastTime;
            }

            if (todayLast.体重 != LastWeight)
            {
                todayLast.体重 = LastWeight;
            }

            todayLast.Update();

            var todayFirst = todayList.Count() > 1 ? todayList.ToList()[1] : null;
            if (todayFirst == null)
            {
                var sequenceNumber = WeightHistory.GetMaxSequenceNumber() + 1;
                todayFirst = new WeightHistory
                {
                    患者id = PatientNumber,
                    患者体重歴連番号 = sequenceNumber,
                    計測日 = todate
                };
                todayFirst.Insert();
            }

            if (todayFirst.計測時間 != FirstTime)
            {
                todayFirst.計測時間 = FirstTime;
            }

            if (todayFirst.体重 != FirstWeight)
            {
                todayFirst.体重 = FirstWeight;
            }

            todayFirst.Update();
        }

        private QueryPatientInfoResponse GetPatientInfo(string pid, bool validateMessage = true, TcpClient client = null, NetworkStream stream = null)
        {
            var request = new QueryPatientInfoRequest();
            var msg = request.CreateRequestMessage(pid);

            msg.HL7Message = msg.SerializeMessage(false);

            // 送信するメッセージを確認
            var msgstr = msg.SerializeMessage(true);

            if (client == null || !client.Connected)
            {
                client = new TcpClient(server, port);
            }

            if (stream == null)
            {
                stream = client.GetStream();
            }

            var encodeddata = Encoding.GetEncoding(50222).GetBytes(msgstr);
            var senddata = WrapHL7HeaderTrailer(encodeddata);

            stream.Write(senddata, 0, senddata.Length);
            stream.Flush();

            // 返信を受信
            var recvdata = new byte[8192];
            var recvLength = stream.Read(recvdata, 0, recvdata.Length);
            var recvBuffer = CheckAndExtractRecvHL7Message(recvdata, recvLength);
            var responseData = Encoding.GetEncoding(50222).GetString(recvBuffer).Replace('\0', ' ').Trim();

            // 解析
            var message = new Message(responseData);
            if (!message.ParseMessage())
            {
                Assert.Fail("解析失敗");
            }
            var response = new QueryPatientInfoResponse();
            response.ParseMessage(message, validateMessage);

            return response;
        }

        private byte[] WrapHL7HeaderTrailer(byte[] body)
        {
            var header = new byte[] { 0x0B };
            var trailer = new byte[] { 0x1c, 0x0d };

            var wrapperBuffer = new byte[body.Length + 3];
            Array.Copy(header, 0, wrapperBuffer, 0, 1);
            Array.Copy(body, 0, wrapperBuffer, 1, body.Length);
            Array.Copy(trailer, 0, wrapperBuffer, 1 + body.Length, 2);

            return wrapperBuffer;
        }

        private byte[] CheckAndExtractRecvHL7Message(byte[] data, int length)
        {
            // ゼロデータを切り取り
            var truncatedBuffer = new byte[length];
            if (length <= 0)
            {
                return null;
            }

            Array.Copy(data, truncatedBuffer, length);

            // ヘッダ&トレーラチェック
            if (truncatedBuffer.Length > 4 &&
                (truncatedBuffer[0] != 0x0B || truncatedBuffer[truncatedBuffer.Length - 1] != 0x0D || truncatedBuffer[truncatedBuffer.Length - 2] != 0x1C))
            {
                return null;
            }

            var newBuffer = new byte[truncatedBuffer.Length - 3];
            Array.Copy(truncatedBuffer, 1, newBuffer, 0, truncatedBuffer.Length - 3);
            return newBuffer;
        }

        [TestMethod]
        public void TestGetPatientID()
        {
            var response = GetPatientInfo(PatientNumber.ToString());
            response.ResponseStatus.ShouldBe("AA");
            response.Results.First().Value.PatientID.ShouldBe(PatientNumber.ToString());
        }

        [TestMethod]
        public void TestGetPatientLastName()
        {
            var response = GetPatientInfo(PatientNumber.ToString());
            response.ResponseStatus.ShouldBe("AA");
            response.Results.First().Value.PatientLastName.ShouldBe(KanjiLastName);
        }

        [TestMethod]
        public void TestGetPatientFirstName()
        {
            var response = GetPatientInfo(PatientNumber.ToString());
            response.ResponseStatus.ShouldBe("AA");
            response.Results.First().Value.PatientFirstName.ShouldBe(KanjiFirstName);
        }

        [TestMethod]
        public void TestGetPatientLastNameKana()
        {
            var response = GetPatientInfo(PatientNumber.ToString());
            response.ResponseStatus.ShouldBe("AA");
            response.Results.First().Value.PatientLastKana.ShouldBe(KanaLastName);
        }

        [TestMethod]
        public void TestGetPatientFirstNameKana()
        {
            var response = GetPatientInfo(PatientNumber.ToString());
            response.ResponseStatus.ShouldBe("AA");
            response.Results.First().Value.PatientFirstKana.ShouldBe(KanaFirstName);
        }

        [TestMethod]
        public void TestGetLatestHeightEmpty()
        {
            SetupHeightEmpty();
            var response = GetPatientInfo(PatientNumber.ToString());
            response.ResponseStatus.ShouldBe("AA");
            response.Results.First().Value.Height.ShouldBeNull();
        }

        [TestMethod]
        public void TestGetLatestHeightData()
        {
            var response = GetPatientInfo(PatientNumber.ToString());
            response.ResponseStatus.ShouldBe("AA");
            response.Results.First().Value.Height.ShouldBe(LastHeight.ToString());
        }

        [TestMethod]
        public void TestGetLatestWeightEmpty()
        {
            SetupWeightEmpty();
            var response = GetPatientInfo(PatientNumber.ToString());
            response.ResponseStatus.ShouldBe("AA");
            response.Results.First().Value.Weight.ShouldBeNull();
        }

        [TestMethod]
        public void TestGetLatestWeightData()
        {
            var response = GetPatientInfo(PatientNumber.ToString());
            response.ResponseStatus.ShouldBe("AA");
            response.Results.First().Value.Weight.ShouldBe(LastWeight.ToString());
        }

        [TestMethod]
        public void TestInvalidPatientNumber()
        {
            var response = GetPatientInfo((PatientNumber+1).ToString(), validateMessage: false);
            response.ResponseStatus.ShouldBe("AE");
        }

        [TestMethod]
        public void TestBlankPatientNumber()
        {
            var response = GetPatientInfo(string.Empty, validateMessage: false);
            response.ResponseStatus.ShouldBe("AE");
        }

        [TestMethod]
        public void TestZenkakuNumericPatientNumber()
        {
            var response = GetPatientInfo(ZPatientNumber, validateMessage: false);
            response.ResponseStatus.ShouldBe("AA");
            response.Results.First().Value.PatientID.ShouldBe(ZPatientNumber);
        }

        [TestMethod]
        public void TestNonNumericPatientNumber()
        {
            var response = GetPatientInfo("A01", validateMessage: false);
            response.ResponseStatus.ShouldBe("AE");
        }

        [TestMethod]
        public void TestZenkakuPatientNumber()
        {
            var response = GetPatientInfo("Ａ０１", validateMessage: false);
            response.ResponseStatus.ShouldBe("AE");
        }

        [TestMethod]
        public void TestBloodTypeEmpty()
        {
            SetupBloodTypeEmpty();
            var response = GetPatientInfo(PatientNumber.ToString());
            response.ResponseStatus.ShouldBe("AA");
            response.Results.First().Value.BloodType.ShouldBeNull();
        }

        [TestMethod]
        public void TestBloodType()
        {
            var response = GetPatientInfo(PatientNumber.ToString());
            response.ResponseStatus.ShouldBe("AA");
            response.Results.First().Value.BloodType.ShouldBe(BloodType);
        }

        [TestMethod]
        public void TestBirthDate()
        {
            var response = GetPatientInfo((PatientNumber).ToString());
            response.ResponseStatus.ShouldBe("AA");
            response.Results.First().Value.PatientBirthDate.ShouldBe(BirthDate);
        }

        [TestMethod]
        public void TestGender()
        {
            var response = GetPatientInfo((PatientNumber).ToString());
            response.ResponseStatus.ShouldBe("AA");
            response.Results.First().Value.PatientGender.ShouldBe(GenderCode == 1 ? "M" : GenderCode == 2 ? "F" : "U");
        }

        [TestMethod]
        public void TestClientTimeout()
        {
            var client = new TcpClient(server, port);
            var stream = client.GetStream();
            stream.ReadTimeout = 10000; // 10秒

            var response = GetPatientInfo(PatientNumber.ToString(), client: client, stream: stream);
            response.ResponseStatus.ShouldBe("AA");
            response.Results.First().Value.PatientID.ShouldBe(PatientNumber.ToString());

            Thread.Sleep(11000);

            try
            {
                response = GetPatientInfo(PatientNumber.ToString(), client: client, stream: stream);
                response.ResponseStatus.ShouldBe("AA");
                response.Results.First().Value.PatientID.ShouldBe(PatientNumber.ToString());
            }
            catch (Exception e) when (!(e?.InnerException is SocketException sex) || sex.SocketErrorCode != SocketError.TimedOut)
            {
            }

            // やり直し
            response = GetPatientInfo(PatientNumber.ToString());
            response.ResponseStatus.ShouldBe("AA");
            response.Results.First().Value.PatientID.ShouldBe(PatientNumber.ToString());
        }
    }
}
