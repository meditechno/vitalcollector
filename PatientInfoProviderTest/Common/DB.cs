﻿///////////////////////////////////////////////////////
// Copyright (c) 2020 PHC株式会社
// Filename: DB.cs
///////////////////////////////////////////////////////
namespace PatientInfoProviderTest.Common
{
    using Dapper;
    using Npgsql;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;

    public class DBSetting
    {
        public string Address { get; set; }
        public string PortNumber { get; set; } = "5432";
        public string DatabaseName { get; set; }
        public string Encoding { get; set; } = "UTF8";
        public string Username { get; set; }
        public string Password { get; set; }
    }
    public class DB : IDisposable
    {
        private readonly string ConnectionStr = string.Empty;

        private static DBSetting dbSetting = null;

        /// <summary>
        /// コンストラクタ。設定をロードします
        /// </summary>
        /// <returns></returns>
        public DB (DBSetting setting)
        {
            dbSetting = setting;
            ConnectionStr = $"Server={setting.Address};Port={setting.PortNumber};Database={setting.DatabaseName};Encoding={setting.Encoding};User Id={setting.Username};Password={setting.Password};";
        }

        public DB()
        {
            if (dbSetting == null)
            {
                throw new InvalidOperationException();
            }
            ConnectionStr = $"Server={dbSetting.Address};Port={dbSetting.PortNumber};Database={dbSetting.DatabaseName};Encoding={dbSetting.Encoding};User Id={dbSetting.Username};Password={dbSetting.Password};";
        }

        /// <summary>
        /// トランザクション
        /// </summary>
        public class Transaction : IDisposable
        {
            private readonly DB database;
            public NpgsqlConnection MyConnection { get; private set; }
            public NpgsqlTransaction MyTransaction { get; private set; }

            public Transaction(DB database)
            {
                this.database = database;
                Open();
                MyTransaction = MyConnection.BeginTransaction();
            }

            private bool Open()
            {
                MyConnection = new NpgsqlConnection(database.ConnectionStr);
                try
                {
                    MyConnection.Open();
                }
                catch (Exception)
                {
                    return false;
                }

                if (MyConnection.FullState != System.Data.ConnectionState.Open)
                {
                    return false;
                }

                return true;
            }

            internal void Commit()
            {
                MyTransaction.Commit();
            }

            internal void Rollback()
            {
                MyTransaction.Rollback();
            }

            public void Dispose()
            {
                MyConnection.Close();
                MyTransaction.Dispose();
                MyConnection.Dispose();
            }
        }

        /// <summary>
        /// トランザクションを開始します
        /// </summary>
        /// <returns></returns>
        public Transaction CreateTransaction()
        {
            return new Transaction(this);
        }

        /// <summary>
        /// SQLコマンドを作成します。
        /// </summary>
        /// <param name="commandText"></param>
        /// <returns></returns>
        public Command CreateCmd(string commandText, Transaction tran)
        {
            return new Command(this, commandText, tran);
        }

        /// <summary>
        /// SQLコマンドを作成します。
        /// </summary>
        /// <param name="commandText">コマンドtext</param>
        /// <returns></returns>
        public Command CreateCmd(string commandText)
        {
            return new Command(this, commandText);
        }

        public class Command : IDisposable
        {
            private readonly DB database;
            private readonly NpgsqlCommand command;
            private NpgsqlConnection connection;

            private bool Open()
            {
                if (connection != null)
                {
                    connection.Close();
                    connection.Dispose();
                }
                connection = new NpgsqlConnection(database.ConnectionStr);
                try
                {
                    connection.Open();
                }
                catch (Exception)
                {
                    return false;
                }

                if (connection.FullState != System.Data.ConnectionState.Open)
                {
                    return false;
                }

                return true;
            }

            private bool ReOpen()
            {
                if (!Open())
                {
                    return false;
                }

                command.Connection = connection;
                return true;
            }

            public Command(DB db, string commandText)
            {
                this.database = db;
                Open();
                command = new NpgsqlCommand(commandText, connection);
            }

            public Command(DB db, string commandText, Transaction tran)
            {
                this.database = db;
                command = new NpgsqlCommand(commandText, tran.MyConnection, tran.MyTransaction);
            }

            /// <summary>
            /// このコマンドによって使用されているすべてのリソースを解放します。
            /// </summary>
            public void Dispose()
            {
                if (command.Transaction == null)
                {
                    connection?.Close();
                    connection?.Dispose();
                }
                command.Dispose();
            }

            /// <summary>
            /// Tryを含んだExcuteReaderを発行し、リストで返します。失敗した場合 null が返ります。
            /// </summary>
            /// <param name="cmd"></param>
            /// <returns></returns>
            public List<object[]> TryExecuteReaderList()
            {
                var lst = new List<object[]>();

                try
                {
                    using (var dr = command.ExecuteReader())
                    {
                        var c = dr.FieldCount;
                        while (dr.Read())
                        {
                            var ojs = new object[c];
                            dr.GetValues(ojs);
                            lst.Add(ojs);
                        }
                    }
                    return lst;
                }
                catch (Exception ex)
                {
                    if (command.Transaction != null) throw ex;
                    if (!ReOpen()) throw ex;
                }

                try
                {
                    using (var dr = command.ExecuteReader())
                    {
                        var c = dr.FieldCount;
                        while (dr.Read())
                        {
                            var ojs = new object[c];
                            dr.GetValues(ojs);
                            lst.Add(ojs);
                        }
                    }
                    return lst;
                }
                catch (Exception ex)
                {
                    var sfm = new System.Diagnostics.StackFrame(1).GetMethod();
                    var data = "Class:" + sfm.DeclaringType.FullName + " Method:" + sfm.Name;
                    ex.Data.Add("MethodName", data);
                    throw ex;
                }
            }

            /// <summary>
            /// Tryを含んだExecuteScalarを発行します。失敗した場合 DBNull.Value が返ります。
            /// </summary>
            /// <param name="cmd"></param>
            /// <returns></returns>
            public object TryExecuteScalar()
            {
                try
                {
                    return command.ExecuteScalar();
                }
                catch (Exception ex)
                {
                    if (command.Transaction != null) throw ex;
                    if (!ReOpen()) throw ex;
                }

                try
                {
                    return command.ExecuteScalar();
                }
                catch (Exception ex)
                {
                    var sfm = new System.Diagnostics.StackFrame(1).GetMethod();
                    var data = "Class:" + sfm.DeclaringType.FullName + " Method:" + sfm.Name;
                    ex.Data.Add("MethodName", data);
                    throw ex;
                }
            }

            /// <summary>
            /// Tryを含んだExecuteNonQueryを発行します。失敗した場合 false が返ります。
            /// </summary>
            /// <param name="cmd"></param>
            /// <returns></returns>
            public bool TryExecuteNonQuery()
            {
                try
                {
                    command.ExecuteNonQuery();
                    return true;
                }
                catch (Exception ex)
                {
                    if (command.Transaction != null) throw ex;
                    if (!ReOpen()) throw ex;
                }

                try
                {
                    command.ExecuteNonQuery();
                    return true;
                }
                catch (Exception ex)
                {
                    var sfm = new System.Diagnostics.StackFrame(1).GetMethod();
                    var data = "Class:" + sfm.DeclaringType.FullName + " Method:" + sfm.Name;
                    ex.Data.Add("MethodName", data);
                    throw ex;
                }
            }
        }

        /////////////////////////////////////////////////////////////
        //
        //   ここから先、Dapper使用のコマンド拡張
        //
        /////////////////////////////////////////////////////////////

        private NpgsqlConnection GetOpenConnection()
        {
            return new NpgsqlConnection(ConnectionStr);
        }

        public IEnumerable<T> Query<T>(string sql, object ps)
        {
            try
            {
                using (var con = GetOpenConnection())
                {
                    var res = con.Query<T>(sql, ps);
                    return res;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool Execute(string sql, object obj, Transaction transaction = null)
        {
            var connection = transaction == null ? GetOpenConnection() : transaction.MyConnection;

            try
            {
                connection.Execute(sql, obj, transaction?.MyTransaction);
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (transaction == null) connection.Dispose();
            }
            return true;
        }

        /// <summary>
        /// 全てのデータを取得します
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public IEnumerable<T> SelectAll<T>()
        {
            var sql = SqlBuilder.CreateSelectSql<T>();
            using (var con = GetOpenConnection())
            {
                return con.Query<T>(sql);
            }
        }

        /// <summary>
        /// WHERE句を直接指定して、データをSELECTします
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="where">WHERE句 "WHERE"は不要</param>
        /// <returns></returns>
        public IEnumerable<T> Select<T>(string where)
        {
            var sql = SqlBuilder.CreateSelectSql<T>(where);
            using (var con = GetOpenConnection())
            {
                return con.Query<T>(sql);
            }
        }
            
        /// <summary>
        /// 匿名クラスを利用し、データをSELECTします
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="whereClass">条件指定クラス　example: new { id = id, name = "test" }</param>
        /// <returns></returns>
        public IEnumerable<T> Select<T>(object whereClass)
        {
            var sql = SqlBuilder.CreateSelectSql<T>(whereClass);
            using (var con = GetOpenConnection())
            {
                return con.Query<T>(sql, whereClass);
            }
        }

        /// <summary>
        /// データをInsertします
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <param name="transaction"></param>
        /// <returns></returns>
        public void Insert<T>(T obj, Transaction transaction = null)
        {
            var sql = SqlBuilder.CreateInsertSql<T>();
            var connection = transaction == null ? GetOpenConnection() : transaction.MyConnection;

            try
            {
                connection.Execute(sql, obj);
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (transaction == null) connection.Dispose();
            }
        }

        /// <summary>
        /// データをInsertし、アサインされたIDを返します
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <param name="tran"></param>
        /// <returns></returns>
        public int Insert<T>(T obj, string keyName, Transaction tran = null)
        {
            var sql = $"{SqlBuilder.CreateInsertSql<T>(keyName)} RETURNING {keyName}";
            var con = tran == null ? GetOpenConnection() : tran.MyConnection;

            try
            {
                return (int)con.ExecuteScalar(sql, obj);
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (tran == null) con.Dispose();
            }
        }

        /// <summary>
        /// データ群をINSERTします
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="objs"></param>
        /// <param name="transaction"></param>
        /// <returns></returns>
        public bool InsertBatch<T>(IEnumerable<T> objs, Transaction transaction = null)
        {
            var sql = SqlBuilder.CreateInsertSql<T>();
            var connection = transaction == null ? GetOpenConnection() : transaction.MyConnection;

            try
            {
                connection.Execute(sql, objs);
                return true;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (transaction == null) connection.Dispose();
            }
        }

        /// <summary>
        /// Key属性があらかじめ指定されたプロパティを条件にデータをUPDATEします
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <param name="transaction"></param>
        /// <returns></returns>
        public bool Update<T>(T obj, Transaction transaction = null)
        {
            var sql = SqlBuilder.CreateUpdateSql<T>();
            var connection = transaction == null ? GetOpenConnection() : transaction.MyConnection;

            try
            {
                connection.Execute(sql, obj);
                return true;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (transaction == null) connection.Dispose();
            }
        }

        /// <summary>
        /// Key属性があらかじめ指定されたプロパティを条件にデータ群をUPDATEします
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <param name="transaction"></param>
        /// <returns></returns>
        public bool Updates<T>(IEnumerable<T> objs, Transaction transaction = null)
        {
            var sql = SqlBuilder.CreateUpdateSql<T>();
            var connection = transaction == null ? GetOpenConnection() : transaction.MyConnection;

            try
            {
                connection.Execute(sql, objs);
                return true;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (transaction == null) connection.Dispose();
            }
        }

        public int GetMaxId<T>(string key, Transaction tran = null)
        {
            var sql = SqlBuilder.CreateMaxIdSql<T>(key);
            var con = tran == null ? GetOpenConnection() : tran.MyConnection;

            try
            {
                return (int)con.ExecuteScalar(sql);
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (tran == null) con.Dispose();
            }
        }

        public void Dispose()
        {
            //if ()
        }

        /// <summary>
        /// データベースのカラムに関連した属性を管理します
        /// </summary>
        public class Attribute
        {
            /// <summary>
            /// データベース上のテーブルでキーとなるプロパティに指定します
            /// </summary>
            [AttributeUsage(AttributeTargets.Property, AllowMultiple = true, Inherited = false)]
            public class PrimaryKey : System.Attribute { }

            /// <summary>
            /// データベース上のテーブルにないプロパティに指定します
            /// </summary>
            [AttributeUsage(AttributeTargets.Property, AllowMultiple = true, Inherited = false)]
            public class Ignore : System.Attribute { }

            /// <summary>
            /// プロパティの種別をマッピングします
            /// </summary>
            [AttributeUsage(AttributeTargets.Property, AllowMultiple = true, Inherited = false)]
            public class MapProperty : System.Attribute {
                public readonly Type MapType1;
                public readonly Type MapType2;
                public MapProperty(Type type1, Type type2)
                {
                    MapType1 = type1;
                    MapType2 = type2;
                }
            }


            /// <summary>
            /// クラス名以外のテーブルに関連付けする場合に指定します
            /// </summary>
            [AttributeUsage(AttributeTargets.Class, AllowMultiple = true, Inherited = false)]
            public class DifferentTableName : System.Attribute
            {
                public readonly string TableName;
                public DifferentTableName(string tableName) { TableName = tableName; }
            }
        }

        /// <summary>
        /// SQL文をクラスから自動生成します
        /// </summary>
        public class SqlBuilder
        {
            /// <summary>
            /// クラス情報をリフレクションにより取得し、キャッシュします
            /// </summary>
            class ClassCache
            {
                private static readonly Dictionary<Type, ClassCache> classes = new Dictionary<Type, ClassCache>();

                public string TableName { get; private set; }
                public List<string> UpdateColumns { get; private set; } = new List<string>();
                public List<string> Columns { get; private set; } = new List<string>();
                public List<string> Keys { get; private set; } = new List<string>();

                readonly PropertyInfo[] properties;

                ClassCache(Type t)
                {
                    var ta = Array.Find(System.Attribute.GetCustomAttributes(t), x => x is Attribute.DifferentTableName);
                    TableName = ((Attribute.DifferentTableName)ta)?.TableName ?? t.Name;
                    properties = t.GetProperties();

                    foreach (var item in properties)
                    {
                        if (Array.Exists(System.Attribute.GetCustomAttributes(item), x => x is Attribute.Ignore))
                            continue;

                        Columns.Add(item.Name);
                        if (Array.Exists(System.Attribute.GetCustomAttributes(item), x => x is Attribute.PrimaryKey))
                            Keys.Add(item.Name);
                        else
                            UpdateColumns.Add(item.Name);
                    }
                }

                public static ClassCache GetCache(Type t)
                {
                    return classes.ContainsKey(t) ?
                        classes[t] : new ClassCache(t);
                }
            }

            public static string CreateMaxIdSql<T>(string key)
            {
                var c = ClassCache.GetCache(typeof(T));

                return $"SELECT MAX({key}) FROM {c.TableName}";
            }

            public static string CreateSelectSql<T>()
            {
                var c = ClassCache.GetCache(typeof(T));
                return $"SELECT {string.Join(",", c.Columns)} FROM {c.TableName};";
            }

            public static string CreateSelectSql<T>(string where)
            {
                var c = ClassCache.GetCache(typeof(T));
                return $"SELECT {string.Join(",", c.Columns)} FROM {c.TableName} WHERE {where};";
            }

            public static string CreateSelectInnerJoinSql<T, T2>(string keyName, string where, string keyName2 = null)
            {
                var c = ClassCache.GetCache(typeof(T));
                var c2 = ClassCache.GetCache(typeof(T2));

                var cols = c.Columns.ConvertAll(delegate (string name) { return $"a.{name}"; });
                var cols2 = c2.Columns.ConvertAll(delegate (string name) { return $"b.{name}"; });
                return $"SELECT {string.Join(",", cols)}, {string.Join(",", cols2)} FROM {c.TableName} AS a INNER JOIN {c2.TableName} AS b ON a.{keyName} = b.{(keyName2 ?? keyName)} WHERE {where};";
            }

            public static string CreateSelectSql<T>(object obj)
            {
                var c = ClassCache.GetCache(typeof(T));

                var t = obj.GetType();
                if (!t.IsClass) throw new Exception("指定されたオブジェクトはクラスではありません");
                var ps = t.GetProperties();

                return $@"SELECT {string.Join(",", c.Columns)} FROM {c.TableName} 
                          WHERE {string.Join(" AND ", ps.Select(p => $"{p.Name}=@{p.Name}"))};";
            }


            public static string CreateInsertSql<T>(string key = null)
            {
                var c = ClassCache.GetCache(typeof(T));

                if (key != null)
                {
                    c.Columns.RemoveAll(n => n.Equals(key, StringComparison.OrdinalIgnoreCase));
                }
                return $@"INSERT INTO {c.TableName}({string.Join(",", c.Columns)})
                          VALUES(@{string.Join(",@", c.Columns)})";
            }

            public static string CreateUpdateSql<T>()
            {
                var c = ClassCache.GetCache(typeof(T));
                if (c.Keys.Count == 0) throw new Exception("キー属性が指定されていません");

                return $@"UPDATE {c.TableName} SET {string.Join(",", c.UpdateColumns.Select(p => $"{p}=@{p}"))} 
                          WHERE {string.Join(" AND ", c.Keys.Select(p => $"{p}=@{p}"))};";
            }
        }
    }
}