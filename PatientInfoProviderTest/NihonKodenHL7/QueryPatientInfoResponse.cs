﻿///////////////////////////////////////////////////////
// Copyright (c) 2019 PHC株式会社
// Filename: QueryPatientInfoResponse.cs
///////////////////////////////////////////////////////

namespace PatientInfoProviderTest.NihonKodenHL7
{
    using HL7.Dotnetcore;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Security.Cryptography.X509Certificates;

    internal enum Status { None = 0, QRD = 1, PID = 2, OBX = 3 };

    public class QueryPatientInfoResponse
    {
        private const string MessageType = "ADR^A19^ADR_A19";
        private const string ProcessID = "P";
        private const string Version = "2.4";
        private const string ResponseType = "NE";
        private const string ApplicationResponseType = "AL";

        public Dictionary<string, PatientResult> Results { get; set; } = new Dictionary<string, PatientResult>();
        public string ResponseStatus { get; set; }

        public void ParseMessage(Message message, bool validate = true)
        {
            this.ValidateResponseMessage(message);

            var errorMessage = string.Empty;
            Status status = Status.None;
            var pid = message.Segments()?.Any(x => x.Name == "PID") == true ? message.GetValue("PID.3") : null;

            if (validate)
            {
                if (!int.TryParse(pid, out _))
                {
                    errorMessage += $"患者番号{pid}が不正です{Environment.NewLine}";
                }
            }

            foreach (var segment in message.Segments())
            {
                switch (status)
                {
                    case Status.None:
                        if (segment.Name == "MSA")
                        {
                            ResponseStatus = segment.Fields(1).Value;
                            status = Status.QRD;
                        }
                        break;
                    case Status.QRD:
                        if (segment.Name == "QRD")
                        {
                            status = Status.PID;
                        }
                        break;
                    case Status.PID:
                        if (segment.Name == "PID")
                        {
                            if (!Results.TryGetValue(pid, out var patient))
                            {
                                patient = new PatientResult
                                {
                                    PatientID = pid,
                                    PatientLastName  = UnwrapEscapedName(segment.Fields(5).Repetitions().SingleOrDefault(x => x.Components().Last().Value == "I")?.Components(1).Value),
                                    PatientFirstName = UnwrapEscapedName(segment.Fields(5).Repetitions().SingleOrDefault(x => x.Components().Last().Value == "I")?.Components(2).Value),
                                    PatientLastKana  = UnwrapEscapedName(segment.Fields(5).Repetitions().SingleOrDefault(x => x.Components().Last().Value == "P")?.Components(1).Value),
                                    PatientFirstKana = UnwrapEscapedName(segment.Fields(5).Repetitions().SingleOrDefault(x => x.Components().Last().Value == "P")?.Components(2).Value),
                                    PatientBirthDate = segment.Fields(7).Value,
                                    PatientGender = segment.Fields(8).Value
                                };
                                Results.Add(pid, patient);
                            }

                            status = Status.OBX;
                        }
                        break;
                    case Status.OBX:
                        if (segment.Name == "OBX")
                        {
                            if (!Results.TryGetValue(pid, out var patient))
                            {
                                errorMessage += $"患者情報を照合できません [{pid}]";
                                continue;
                            }

                            var value = segment.Fields(5).Value;
                            if (segment.Fields(3).Value.Contains("Height"))
                            {
                                patient.Height = value;
                            }
                            else if (segment.Fields(3).Value.Contains("Weight"))
                            {
                                patient.Weight = value;
                            }
                            else if (segment.Fields(3).Value.Contains("Blood"))
                            {
                                patient.BloodType = value;
                            }
                        }
                        break;
                }
            }

            if (!string.IsNullOrEmpty(errorMessage))
            {
                throw new ArgumentException(errorMessage);
            }
        }

        private string UnwrapEscapedName(string name)
        {
            const string Start = "\\X01B\\$B";
            const string End = "\\X01B\\(B";
            var processedName = name;
            if (name.StartsWith(Start))
            {
                processedName = name.Replace(Start, "");
            }

            if (name.EndsWith(End))
            {
                processedName = processedName.Replace(End, "");
            }
            return processedName;
        }

        private void ValidateResponseMessage(Message message)
        {
            var messageType = message.GetValue("MSH.9");
            if (messageType != MessageType)
            {
                throw new ArgumentException($"MSHメッセージタイプが不正です[{messageType}]");
            }

            var processID = message.GetValue("MSH.11");
            if (processID != ProcessID)
            {
                throw new ArgumentException($"MSH処理IDが不正です[{processID}]");
            }

            var version = message.GetValue("MSH.12");
            if (version != Version)
            {
                throw new ArgumentException($"MSHバージョンが不正です[{version}]");
            }

            var responseType = message.GetValue("MSH.15");
            if (responseType != ResponseType)
            {
                throw new ArgumentException($"MSH受諾応答型が不正です[{responseType}]");
            }

            var applicationResponseType = message.GetValue("MSH.16");
            if (applicationResponseType != ApplicationResponseType)
            {
                throw new ArgumentException($"MSHアプリケーション応答型が不正です[{applicationResponseType}]");
            }

            //var positiveResponse = message.GetValue("MSA.1");
            //if (positiveResponse != PositiveResponseCodeA && positiveResponse != PositiveResponseCodeC)
            //{
            //    throw new ArgumentException($"MSA肯定応答コードが不正です[{positiveResponse}]");
            //}
        }
    }

    public class PatientResult
    {
        internal string PatientID { get; set; }
        internal string PatientLastName { get; set; }
        internal string PatientFirstName { get; set; }
        internal string PatientLastKana { get; set; }
        internal string PatientFirstKana { get; set; }
        internal string PatientBirthDate { get; set; }
        internal string PatientGender { get; set; }
        internal string Height { get; set; }
        internal string Weight { get; set; }
        internal string BloodType { get; set; }
    }
}
