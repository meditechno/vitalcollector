﻿///////////////////////////////////////////////////////
// Copyright (c) 2019 PHC株式会社
// Filename: QueryPatientInfoRequest.cs
///////////////////////////////////////////////////////

namespace PatientInfoProviderTest.NihonKodenHL7
{
    using HL7.Dotnetcore;
    using System;
    using System.Linq;

    /// <summary>
    /// 患者情報の照会（QRY/ADR: イベントA19）
    /// </summary>
    public class QueryPatientInfoRequest
    {
        private const int SequenceNumber = 1;
        private Segment QRD { get; set; }
        public string PatientNumber = "1234567890";

        public Message CreateRequestMessage(string patientNumber)
        {
            this.PatientNumber = patientNumber;
            var message = new Message();

            // MSH
            const string alldelimeter = @"|^~\&";
            var msgstring = $"MSH{alldelimeter}|HL7Gateway|Hospital|VitalCollector|Hospital|{DateTime.Now:yyyyMMddHHmmss}||QRY^A19^QRY_A19|{DateTime.Today:yyyyMMdd}{SequenceNumber:D6}|P|2.4|||NE|AL||ISO IR87||ISO 2022-1994\r";
            var mshmessage = new Message(msgstring);
            mshmessage.ParseMessage();
            message.AddNewSegment(mshmessage.DefaultSegment("MSH"));

            this.CreateQRD();
            message.AddNewSegment(QRD);
            return message;
        }

        private void CreateQRD()
        {
            QRD = new Segment("QRD", new HL7Encoding());

            // QRD-1
            QRD.AddNewField(DateTime.Now.ToString("yyyyMMddHHmmss"));

            // QRD-2
            QRD.AddNewField("R");

            // QRD-3
            QRD.AddNewField("I");

            // QRD-4
            QRD.AddNewField($"{DateTime.Today:MMdd}{SequenceNumber:D6}");

            // QRD-5,6
            QRD.AddEmptyField();
            QRD.AddEmptyField();

            // QRD-7
            QRD.AddNewField("RD^1");

            // QRD-8
            QRD.AddNewField($"{PatientNumber}");

            // QRD-9
            QRD.AddNewField("APN");

            // QRD-10
            QRD.AddEmptyField();
        }
    }
}
