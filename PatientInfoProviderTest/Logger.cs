﻿///////////////////////////////////////////////////////
// Copyright (c) 2019 PHC株式会社
// Filename: Logger.cs
///////////////////////////////////////////////////////
namespace PatientInfoProvider.Common
{
    using System;
    using System.IO;

    /// <summary>
    /// ロガークラス
    /// </summary>
    public class Logger
    {
        public enum Mode { Recv, Send };
        private const string LogFileName = "PatientInfoProvider.log";
        private const string RecvFileName = "RecvLog";
        public static bool DebugMode { get; set; } = false;

        public static void WriteBinaryToLogfile(byte[] data, Mode mode)
        {
            var today = DateTime.Now.ToString("yyyyMMddHHmmss");
            var filename = $"{mode}{today}.log";

            File.WriteAllBytes(filename, data);
        }

        public static void WriteLog(string message, string filename = null, Exception e = null)
        {
            using (var writer = new StreamWriter(LogFileName, true))
            {
                var now = DateTime.Now;
                // デバッグモードによってスタックトレースを出力するかを判断
                var ex = !DebugMode || e == null ? string.Empty : e.StackTrace;
                var fileInfo = string.IsNullOrEmpty(filename) ? string.Empty : $"ファイル[{filename}]";
                writer.WriteLine($"{now.ToString()}: {fileInfo} {message} {ex}");
            }
        }
    }
}
