﻿///////////////////////////////////////////////////////
// Copyright (c) 2020 PHC株式会社
// Filename: WeightHistory.cs
///////////////////////////////////////////////////////

namespace PatientInfoProvider.Data
{
    using PatientInfoProviderTest.Common;
    using System.Collections.Generic;
    using System.Linq;
    using static PatientInfoProviderTest.Common.DB.Attribute;

    [DifferentTableName("連携_患者体重歴")]
    public class WeightHistory
    {
        [PrimaryKey]
        public int 患者id { get; set; }
        [PrimaryKey]
        public int 患者体重歴連番号 { get; set; }
        public double 体重 { get; set; }
        public string 計測日 { get; set; }
        public string 計測時間 { get; set; }

        public static IEnumerable<WeightHistory> GetWeightDatas(int pid, string day)
        {
            IEnumerable<WeightHistory> result = null;
            using (var db = new DB())
            {
                result = db.Select<WeightHistory>(new { 患者id = pid, 計測日 = day });
            }
            return result;
        }

        public static WeightHistory GetLatestWeight(int pid)
        {
            WeightHistory result = null;
            using (var db = new DB())
            {
                result = db.Select<WeightHistory>(new { 患者id = pid })
                    .OrderByDescending(x => x.計測日)
                    .ThenByDescending(x => x.計測時間)
                    .FirstOrDefault();
            }

            return result;
        }

        public static void DeleteAll(int patientID)
        {
            using (var db = new DB())
            {
                var sql = $"DELETE FROM 連携_患者体重歴 WHERE {nameof(患者id)} = {patientID}";
                db.Execute(sql, null);
            }
        }

        public void Insert()
        {
            using (var db = new DB())
            {
                db.Insert(this);
            }
        }

        public void Update()
        {
            using (var db = new DB())
            {
                db.Update(this);
            }
        }

        public static int GetMaxSequenceNumber()
        {
            int number = 0;
            using (var db = new DB())
            {
                number = db.Query<int>("SELECT COALESCE(MAX(\"患者体重歴連番号\"), 0) FROM \"連携_患者体重歴\"", null).FirstOrDefault();
            }
            return number;
        }
    }
}
