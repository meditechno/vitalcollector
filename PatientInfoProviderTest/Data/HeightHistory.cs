﻿///////////////////////////////////////////////////////
// Copyright (c) 2020 PHC株式会社
// Filename: HeightHistory.cs
///////////////////////////////////////////////////////

namespace PatientInfoProvider.Data
{
    using PatientInfoProviderTest.Common;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using static PatientInfoProviderTest.Common.DB.Attribute;

    [DifferentTableName("連携_患者身長歴")]
    public class HeightHistory
    {
        [PrimaryKey]
        public int 患者id { get; set; }
        [PrimaryKey]
        public int 患者身長歴連番号 { get; set; }
        public double 身長 { get; set; }
        public string 計測日 { get; set; }
        public string 計測時間 { get; set; }

        public static IEnumerable<HeightHistory> GetHeightDatas(int pid, string day)
        {
            IEnumerable<HeightHistory> result = null;
            using (var db = new DB())
            {
                result = db.Select<HeightHistory>(new { 患者id = pid, 計測日 = day });
            }
            return result;
        }

        public static HeightHistory GetLatestHeight(int pid)
        {
            HeightHistory result = null;
            using (var db = new DB())
            {
                result = db.Select<HeightHistory>(new { 患者id = pid })
                     .OrderBy(x => x.計測日)
                     .ThenBy(x => x.計測時間)
                     .FirstOrDefault();
            }
            return result;
        }

        public static void DeleteAll(int patientID)
        {
            using (var db = new DB())
            {
                var sql = $"DELETE FROM 連携_患者身長歴 WHERE {nameof(患者id)} = {patientID}";
                db.Execute(sql, null);
            }
        }

        public void Insert()
        {
            using (var db = new DB())
            {
                db.Insert(this);
            }
        }

        public void Update()
        {
            using (var db = new DB())
            {
                db.Update(this);
            }
        }

        public static int GetMaxSequenceNumber()
        {
            int number = 0;
            using (var db = new DB())
            {
                number = db.Query<int>("SELECT COALESCE(MAX(\"患者身長歴連番号\"), 0) FROM \"連携_患者身長歴\"", null).FirstOrDefault();
            }
            return number;
        }
    }
}
