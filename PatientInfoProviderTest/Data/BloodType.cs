﻿///////////////////////////////////////////////////////
// Copyright (c) 2020 PHC株式会社
// Filename: BloodType.cs
///////////////////////////////////////////////////////
namespace PatientInfoProvider.Data
{
    /// <summary>
    /// 血液型クラス
    /// </summary>
    public static class BloodType
    {
        public static string GetBloodTypeById(int id)
        {
            var type = string.Empty;
            switch (id)
            {
                case 1:
                    type = "A+";
                    break;
                case 2:
                    type = "B+";
                    break;
                case 3:
                    type = "O+";
                    break;
                case 4:
                    type = "AB+";
                    break;
                case 5:
                    type = "A-";
                    break;
                case 6:
                    type = "B-";
                    break;
                case 7:
                    type = "O-";
                    break;
                case 8:
                    type = "AB-";
                    break;
                default:
                    break;
            }

            return type;
        }

        public static int GetBloodType(string type)
        {
            if (type == "A+")
            {
                return 1;
            }
            else if (type == "B+")
            {
                return 2;
            }
            else if (type == "O+")
            {
                return 3;
            }
            else if (type == "AB+")
            {
                return 4;
            }
            else if (type == "A-")
            {
                return 5;
            }
            else if (type == "B-")
            {
                return 6;
            }
            else if (type == "O-")
            {
                return 7;
            }
            else if (type == "AB-")
            {
                return 8;
            }
            return -1;
        }
    }
}
