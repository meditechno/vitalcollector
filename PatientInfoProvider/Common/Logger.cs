﻿///////////////////////////////////////////////////////
// Copyright (c) 2020 PHC株式会社
// Filename: Logger.cs
///////////////////////////////////////////////////////
namespace PatientInfoProvider.Common
{
    using System;
    using System.IO;

    /// <summary>
    /// ロガークラス
    /// </summary>
    public static class Logger
    {
        public enum Mode { Recv, Send };
        private const string LogFileName = "PatientInfoProvider.log";
        public static bool DebugMode { get; set; } = false;
        public static string BaseDirectory { get; set; }

        private static readonly object lockObject = new object();

        public static void WriteBinaryToLogfile(byte[] data, int size, Mode mode)
        {
            if (!string.IsNullOrEmpty(BaseDirectory) && !Directory.Exists(BaseDirectory))
            {
                Directory.CreateDirectory(BaseDirectory);
            }

            var writebytes = new byte[size];
            Array.Copy(data, writebytes, size);
            var today = DateTime.Now.ToString("yyyyMMddHHmmssfff");
            var filename = $"{mode}{today}.log";

            File.WriteAllBytes(Path.Combine(BaseDirectory ?? string.Empty, filename), writebytes);
        }

        public static void WriteLog(string message, string filename = null, Exception e = null)
        {
            // スレッドセーフにする
            lock (lockObject)
            {
                if (!string.IsNullOrEmpty(BaseDirectory) && !Directory.Exists(BaseDirectory))
                {
                    Directory.CreateDirectory(BaseDirectory);
                }

                using var writer = new StreamWriter(Path.Combine(BaseDirectory ?? string.Empty, LogFileName), true);
                var now = DateTime.Now;
                // デバッグモードによってスタックトレースを出力するかを判断
                var ex = !DebugMode || e == null ? string.Empty : e.StackTrace;
                var fileInfo = string.IsNullOrEmpty(filename) ? string.Empty : $"ファイル[{filename}]";
                writer.WriteLine($"{now}: {fileInfo} {message} {ex}");
            }
        }
    }
}
