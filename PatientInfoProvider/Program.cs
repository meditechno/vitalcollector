﻿///////////////////////////////////////////////////////
// Copyright (c) 2019 PHC株式会社
// Filename: Program.cs
///////////////////////////////////////////////////////

namespace PatientInfoProvider
{
    using System;
    using System.Threading;
    using Topshelf;

    internal static class Program
    {
        private static void Main(string[] args)
        {
            if (args is null)
            {
                throw new ArgumentNullException(nameof(args));
            }

            HostFactory.Run(x =>
            {
                x.Service<PatientInfoProviceService>();
                x.EnableServiceRecovery(r => r.RestartService(TimeSpan.FromSeconds(15)));
                x.SetServiceName("PatientInformationProviderService");
                x.StartAutomatically();
            });

            // テスト用。サービスではなく単体実行用
            //var service = new PatientInfoProviceService();
            //service.Start(null);
            //Thread.Sleep(10000000);
        }
    }
}
