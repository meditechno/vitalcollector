﻿///////////////////////////////////////////////////////
// Copyright (c) 2019 PHC株式会社
// Filename: PatientInfoProviceService.cs
///////////////////////////////////////////////////////

namespace PatientInfoProvider
{
    using Microsoft.Extensions.Configuration;
    using PatientInfoProvider.Common;
    using System.IO;
    using System.Net;
    using System.Threading.Tasks;
    using Topshelf;

    public class PatientInfoProviceService : ServiceControl
    {
        private const string ConfigFilename = "AppConfig.json";

        private readonly ConnectionMonitor monitor = new ConnectionMonitor();

        public bool Start(HostControl hostControl)
        {
            var config = GetConfiguration();

            Logger.DebugMode = bool.TryParse(config["AppSettings:DebugMode"], out var debugmode) && debugmode;
            Logger.BaseDirectory = config["AppSettings:SaveLogDirectory"] ?? "log";

            Logger.WriteLog("サービス開始");

            int.TryParse(config["AppSettings:ServerPortNumber"], out var port);
            var ipAddress = IPAddress.Parse(config["AppSettings:ServerIPAddress"]);

            // DBの設定
            var dbsetting = new DBSetting
            {
                Address = config["DBSetting:Address"],
                PortNumber = config["DBSetting:PortNumber"],
                DatabaseName = config["DBSetting:DatabaseName"],
                Encoding = config["DBSetting:Encoding"],
                Username = config["DBSetting:Username"],
                Password = config["DBSetting:Password"]
            };
            var _ = new DB(dbsetting);

            Logger.WriteLog($"設定 IP[{ipAddress}:{port}]");
            Logger.WriteLog($"DB設定 DBServer[{dbsetting.Address}:{dbsetting.PortNumber}] Database[{dbsetting.DatabaseName}] Encoding[{dbsetting.Encoding}] Username[{dbsetting.Username}]");

            monitor.IpAddress = ipAddress;
            monitor.PortNumber = port;
            monitor.SaveHL7ToLog = bool.Parse(config["AppSettings:SaveHL7Data"]);
            monitor.Continue = true;

            // 実行
            Task.Run(() => monitor.Run());
            return true;
        }

        public bool Stop(HostControl hostControl)
        {
            Logger.WriteLog("サービス停止");
            monitor.Continue = false;
            return true;
        }

        private static IConfiguration GetConfiguration()
        {
            return new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile(ConfigFilename)
                .Build();
        }
    }
}
