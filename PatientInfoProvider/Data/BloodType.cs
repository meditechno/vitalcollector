﻿///////////////////////////////////////////////////////
// Copyright (c) 2020 PHC株式会社
// Filename: BloodType.cs
///////////////////////////////////////////////////////
namespace PatientInfoProvider.Data
{
    /// <summary>
    /// 血液型クラス。CKの血液型マスターのIDとセントラルモニタのHL7データの型データを変換する
    /// </summary>
    public static class BloodType
    {
        public static string GetBloodTypeById(int id)
        {
            string type = null;
            switch (id)
            {
                case 1:
                    type = "A+";
                    break;
                case 2:
                    type = "B+";
                    break;
                case 3:
                    type = "O+";
                    break;
                case 4:
                    type = "AB+";
                    break;
                case 5:
                    type = "A-";
                    break;
                case 6:
                    type = "B-";
                    break;
                case 7:
                    type = "O-";
                    break;
                case 8:
                    type = "AB-";
                    break;
                default:
                    break;
            }

            return type;
        }
    }
}
