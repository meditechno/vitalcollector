﻿///////////////////////////////////////////////////////
// Copyright (c) 2020 PHC株式会社
// Filename: BasicPatientInfo.cs
///////////////////////////////////////////////////////

namespace PatientInfoProvider.Data
{
    using PatientInfoProvider.Common;
    using System.Collections.Generic;
    using static PatientInfoProvider.Common.DB.Attribute;

    [DifferentTableName("連携_患者基本情報")]
    public class BasicPatientInfo
    {
        public int 患者id { get; set; }
        public int 患者番号 { get; set; }
        public string 漢字氏名 { get; set; }
        public string かな氏名 { get; set; }
        public int 性別 { get; set; }
        public int 血液型cd { get; set; }
        public string 生年月日 { get; set; }
        public int 死亡区分 { get; set; }
        public string 死亡日 { get; set; }
        public string 死亡時間 { get; set; }
        public string 郵便番号 { get; set; }
        public string 住所 { get; set; }
        public string 住所番地方書 { get; set; }
        public int 主治医職員id { get; set; }
        public int 救護区分 { get; set; }

        public static IEnumerable<BasicPatientInfo> GetPatientInfo(int pid)
        {
            using var db = new DB();
            return db.Select<BasicPatientInfo>(new { 患者番号 = pid });
        }
    }
}
