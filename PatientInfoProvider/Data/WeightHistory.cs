﻿///////////////////////////////////////////////////////
// Copyright (c) 2020 PHC株式会社
// Filename: WeightHistory.cs
///////////////////////////////////////////////////////

namespace PatientInfoProvider.Data
{
    using PatientInfoProvider.Common;
    using System.Linq;
    using static PatientInfoProvider.Common.DB.Attribute;

    [DifferentTableName("連携_患者体重歴")]
    public class WeightHistory
    {
        public int 患者id { get; set; }
        public int 患者体重歴連番号 { get; set; }
        public double 体重 { get; set; }
        public string 計測日 { get; set; }
        public string 計測時間 { get; set; }

        public static WeightHistory GetLatestWeight(int pid)
        {
            using var db = new DB();
            return db.Select<WeightHistory>(new { 患者id = pid })
                .OrderByDescending(x => x.計測日)
                .ThenByDescending(x => x.計測時間)
                .FirstOrDefault();
        }
    }
}
