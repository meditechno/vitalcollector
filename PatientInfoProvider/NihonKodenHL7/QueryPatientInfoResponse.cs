﻿///////////////////////////////////////////////////////
// Copyright (c) 2020 PHC株式会社
// Filename: QueryPatientInfoResponse.cs
///////////////////////////////////////////////////////

namespace PatientInfoProvider.NihonKodenHL7
{
    using HL7.Dotnetcore;

    public class QueryPatientInfoResponse
    {
        public QueryPatientInfoRequest Request { get; set; }

        public string PatientLastname { get; set; } = string.Empty;

        public string PatientFirstname { get; set; } = string.Empty;

        public string PatientLastnameKana { get; set; } = string.Empty;

        public string PatientFirstnameKana { get; set; } = string.Empty;

        public string BirthDate { get; set; } = string.Empty;

        public string Gender { get; set; } = string.Empty;

        public string Height { get; set; } = string.Empty;

        public string Weight { get; set; } = string.Empty;

        public string BloodType { get; set; } = string.Empty;

        public Message CreateResponseMessage()
        {
            var message = new Message();

            // MSH
            const string alldelimeter = @"|^~\&";
            var msgstring = $"MSH{alldelimeter}|HL7Gateway|Hospital|VitalCollector|Hospital|||ADR^A19^ADR_A19|{Request.MessageControlID}|P|2.4|||NE|AL||ISO IR87||ISO 2022-1994\r";
            var mshmessage = new Message(msgstring);
            mshmessage.ParseMessage();
            message.AddNewSegment(mshmessage.DefaultSegment("MSH"));

            // MSA
            var msa = new Segment("MSA", new HL7Encoding());
            msa.AddNewField("AA"); // HL7 肯定応答（Application Accept）
            msa.AddNewField(Request.MessageControlID);
            message.AddNewSegment(msa);

            // QRD
            var qrd = Request.QRD;
            message.AddNewSegment(qrd);

            // PID
            var pid = new Segment("PID", new HL7Encoding());
            pid.AddEmptyField();
            pid.AddEmptyField();
            pid.AddNewField(Request.PatientID);
            pid.AddEmptyField();
            pid.AddNewField($"{PatientLastname}^{PatientFirstname}^^^^^L^I~{PatientLastnameKana}^{PatientFirstnameKana}^^^^^L^P");
            pid.AddEmptyField();
            pid.AddNewField(BirthDate);
            pid.AddNewField(Gender);
            pid.AddEmptyField();
            message.AddNewSegment(pid);

            // PV1
            //var pv1 = new Segment("PV1", new HL7Encoding());
            //pv1.AddEmptyField();
            //pv1.AddNewField("U"); // ひとまずUnknownを設定
            //message.AddNewSegment(pv1);

            // OBX
            if (Height != null)
            {
                var obxH = new Segment("OBX", new HL7Encoding());
                obxH.AddNewField("1");
                obxH.AddNewField("NM");
                obxH.AddNewField("2522^Height");
                obxH.AddEmptyField();
                obxH.AddNewField(Height);
                obxH.AddNewField("cm");
                obxH.AddEmptyField();
                obxH.AddEmptyField();
                obxH.AddEmptyField();
                obxH.AddEmptyField();
                obxH.AddNewField("F");
                obxH.AddEmptyField();
                obxH.AddEmptyField();
                obxH.AddEmptyField();
                obxH.AddEmptyField();
                obxH.AddEmptyField();
                obxH.AddEmptyField();
                message.AddNewSegment(obxH);
            }

            if (Weight != null)
            {
                // OBX
                var obxW = new Segment("OBX", new HL7Encoding());
                obxW.AddNewField("2");
                obxW.AddNewField("NM");
                obxW.AddNewField("2523^Weight");
                obxW.AddEmptyField();
                obxW.AddNewField(Weight);
                obxW.AddNewField("kg");
                obxW.AddEmptyField();
                obxW.AddEmptyField();
                obxW.AddEmptyField();
                obxW.AddEmptyField();
                obxW.AddNewField("F");
                obxW.AddEmptyField();
                obxW.AddEmptyField();
                obxW.AddEmptyField();
                obxW.AddEmptyField();
                obxW.AddEmptyField();
                obxW.AddEmptyField();
                message.AddNewSegment(obxW);
            }

            if (BloodType != null)
            {
                // OBX
                var obxBT = new Segment("OBX", new HL7Encoding());
                obxBT.AddNewField("3");
                obxBT.AddNewField("ST");
                obxBT.AddNewField("520^Blood");
                obxBT.AddEmptyField();
                obxBT.AddNewField(BloodType);
                obxBT.AddEmptyField();
                obxBT.AddEmptyField();
                obxBT.AddEmptyField();
                obxBT.AddEmptyField();
                obxBT.AddEmptyField();
                obxBT.AddNewField("F");
                obxBT.AddEmptyField();
                obxBT.AddEmptyField();
                obxBT.AddEmptyField();
                obxBT.AddEmptyField();
                obxBT.AddEmptyField();
                obxBT.AddEmptyField();
                message.AddNewSegment(obxBT);
            }

            // DG1
            var dg1 = new Segment("DG1", new HL7Encoding());
            dg1.AddNewField("1");
            dg1.AddEmptyField();
            dg1.AddEmptyField();
            dg1.AddEmptyField();
            dg1.AddEmptyField();
            dg1.AddEmptyField();
            dg1.AddEmptyField();
            dg1.AddEmptyField();
            dg1.AddEmptyField();
            dg1.AddEmptyField();
            dg1.AddEmptyField();
            dg1.AddEmptyField();
            dg1.AddEmptyField();
            dg1.AddEmptyField();
            dg1.AddEmptyField();
            dg1.AddEmptyField();
            message.AddNewSegment(dg1);

            return message;
        }

        public Message CreateErrorMessage()
        {
            var message = new Message();

            // MSH
            const string alldelimeter = @"|^~\&";
            var controlId = Request?.MessageControlID ?? string.Empty;
            var msgstring = $"MSH{alldelimeter}|HL7Gateway|Hospital|VitalCollector|Hospital|||ADR^A19^ADR_A19|{controlId}|P|2.4|||NE|AL||ISO IR87||ISO 2022-1994\r";
            var mshmessage = new Message(msgstring);
            mshmessage.ParseMessage();
            message.AddNewSegment(mshmessage.DefaultSegment("MSH"));

            // MSA
            var msa = new Segment("MSA", new HL7Encoding());
            msa.AddNewField("AE"); // HL7 肯定応答（Application Error）
            msa.AddNewField(controlId);
            msa.AddEmptyField();
            msa.AddEmptyField();
            msa.AddEmptyField();
            msa.AddNewField("^^^^^");
            message.AddNewSegment(msa);

            // QRD
            var qrd = Request.QRD;
            if (qrd != null)
            {
                message.AddNewSegment(qrd);
            }

            return message;
        }
    }
}
