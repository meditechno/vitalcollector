﻿///////////////////////////////////////////////////////
// Copyright (c) 2020 PHC株式会社
// Filename: QueryPatientInfoRequest.cs
///////////////////////////////////////////////////////

namespace PatientInfoProvider.NihonKodenHL7
{
    using HL7.Dotnetcore;
    using System.Linq;

    public class QueryPatientInfoRequest
    {
        public string RequestDateTime { get; set; }

        public string PatientID { get; set; }

        public string MessageControlID { get; set; }

        public Segment QRD { get; set; }

        public QueryPatientInfoRequest(Message message)
        {
            RequestDateTime = message?.GetValue("MSH.7");
            MessageControlID = message?.GetValue("MSH.10");
            PatientID = message?.Segments("QRD")?.FirstOrDefault()?.Fields(8)?.Components(1)?.Value;
            QRD = message?.Segments("QRD")?.FirstOrDefault();
        }
    }
}
