﻿///////////////////////////////////////////////////////
// Copyright (c) 2019 PHC株式会社
// Filename: ConnectionMonitor.cs
///////////////////////////////////////////////////////
namespace PatientInfoProvider
{
    using HL7.Dotnetcore;
    using Microsoft.VisualBasic;
    using PatientInfoProvider.Common;
    using PatientInfoProvider.Data;
    using PatientInfoProvider.NihonKodenHL7;
    using System;
    using System.Linq;
    using System.Net;
    using System.Net.Sockets;
    using System.Text;
    using System.Threading;
    using System.Threading.Tasks;

    public class ConnectionMonitor
    {
        public IPAddress IpAddress { get; set; }
        public int PortNumber { get; set; }
        public bool SaveHL7ToLog { get; set; }

        public bool Continue {
            get => _continue;
            set
            {
                _continue = value;
                if (!_continue)
                {
                    listener?.Stop();
                }
            }
        }

        private bool _continue = false;
        private TcpListener listener = null;

        public void Run()
        {
            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
            listener = new TcpListener(IpAddress, PortNumber);
            listener.Start();

            while (Continue)
            {
                var encodings = Encoding.GetEncodings();
                Logger.WriteLog($"接続待機中: server[{IpAddress}] port[{PortNumber}].....");

                var client = listener.AcceptTcpClient();

                //Logger.WriteLog($"接続済: {((IPEndPoint)listener.LocalEndpoint).Address}:{((IPEndPoint)listener.LocalEndpoint).Port} client[{Thread.CurrentThread.ManagedThreadId}]");
                Task.Run(() => CommunicateClient(client));
            }
        }

        /// <summary>
        /// TCP接続後の処理
        /// </summary>
        /// <param name="client"></param>
        private void CommunicateClient(TcpClient client)
        {
            const int BufferSize = 8192;
            using var stream = client.GetStream();
            stream.ReadTimeout = 10000; // 10秒
            byte[] data = new byte[BufferSize];
            var datetime = DateTime.Now.ToString("yyyyMMddHHmmss");

            Logger.WriteLog($"Starting client[{Thread.CurrentThread.ManagedThreadId}{datetime}]");
            while (Continue)
            {
                int bytesRead;
                try
                {
                    bytesRead = stream.Read(data, 0, BufferSize);
                }
                catch (Exception e)
                {
                    Logger.WriteLog($"Error Reading in client[{client}] {e.Message}");
                    stream.Close();
                    break;
                }

                if (bytesRead == 0)
                {
                    Logger.WriteLog($"Connection closed in client[{client}]");
                    break;
                }
                else if (bytesRead < 0)
                {
                    Logger.WriteLog($"Timed out[{stream.ReadTimeout}]");
                    continue;
                }

                //　受信データ保存
                if (SaveHL7ToLog)
                {
                    Logger.WriteBinaryToLogfile(data, bytesRead, Logger.Mode.Recv);
                }

                var parsedBuffer = CheckAndExtractRecvHL7Message(data);
                if (parsedBuffer == null)
                {
                    Logger.WriteLog("Invalid HL7 packet");
                    continue;
                }
                var recvdata = Encoding.GetEncoding(50222).GetString(parsedBuffer).Replace('\0', ' ').Trim();
                var response = new QueryPatientInfoResponse();
                string packet;

                try
                {
                    var message = new Message(recvdata);

                    // 戻り値を見ると拾いたいパターンをこぼしてしまうので，例外が発生しない限り処理続行
                    _ = message.ParseMessage();

                    var display = recvdata.Replace("\r", "\r\n").Trim();
                    Logger.WriteLog($"RECV: {display}{Environment.NewLine}");

                    var request = new QueryPatientInfoRequest(message);
                    Logger.WriteLog($"PatientID:[{request.PatientID}]");
                    response.Request = request;

                    var validID = int.TryParse(request.PatientID.Trim(), out var pid);
                    if (!validID)
                    {
                        var tempRequestID = request.PatientID;
                        // 日本語の半角化
                        tempRequestID = Strings.StrConv(tempRequestID, VbStrConv.Narrow);
                        Logger.WriteLog($"Converted PatientID:[{tempRequestID}]");
                        validID = int.TryParse(tempRequestID.Trim(), out pid);
                    }

                    Logger.WriteLog($"PatientID:GO->{validID}");

                    if (validID)
                    {
                        var patientInfo = BasicPatientInfo.GetPatientInfo(pid).FirstOrDefault();
                        if (patientInfo != null)
                        {
                            var height = HeightHistory.GetLatestHeight(patientInfo.患者id);
                            var weight = WeightHistory.GetLatestWeight(patientInfo.患者id);

                            response.Height = height?.身長.ToString();
                            response.Weight = weight?.体重.ToString();

                            // スペース区切りで性と名を分ける
                            var kanjiNames = patientInfo.漢字氏名.Split("　");
                            if (kanjiNames.Length < 2)
                            {
                                kanjiNames = patientInfo.漢字氏名.Split(" ");
                            }

                            var kanaNames = patientInfo.かな氏名.Split("　");
                            if (kanaNames.Length < 2)
                            {
                                kanaNames = patientInfo.かな氏名.Split(" ");
                            }

                            response.PatientLastname = kanjiNames.Length > 0 ? kanjiNames[0] : string.Empty;
                            response.PatientFirstname = kanjiNames.Length > 1 ? kanjiNames[1] : string.Empty;
                            response.PatientLastnameKana = kanaNames.Length > 0 ? kanaNames[0] : string.Empty;
                            response.PatientFirstnameKana = kanaNames.Length > 1 ? kanaNames[1] : string.Empty;

                            // 型を変換
                            response.BloodType = BloodType.GetBloodTypeById(patientInfo.血液型cd);
                            response.Gender = patientInfo.性別 == 1 ? "M" : patientInfo.性別 == 2 ? "F" : "U";
                            response.BirthDate = patientInfo.生年月日;

                            packet = response.CreateResponseMessage().SerializeMessage(false);
                        }
                        else
                        {
                            Logger.WriteLog("データベース問い合わせに失敗しました");
                            packet = CreateErrorMessage(response);
                        }
                    }
                    else
                    {
                        Logger.WriteLog($"PatientID:[{request.PatientID}]が無効です");
                        packet = CreateErrorMessage(response);
                    }

                    var encodeddata = Encoding.GetEncoding(50222).GetBytes(packet);
                    var senddata = this.WrapHL7HeaderTrailer(encodeddata);

                    if (SaveHL7ToLog)
                    {
                        Logger.WriteBinaryToLogfile(senddata, senddata.Length, Logger.Mode.Send);
                    }

                    stream.Write(senddata);
                }
                catch (Exception ex)
                {
                    Logger.WriteLog($"ERROR Decoding HL7 message: {recvdata}{Environment.NewLine} ex:{ex.Message}", e: ex);
                    try
                    {
                        packet = CreateErrorMessage(response);
                        var encodeddata = Encoding.GetEncoding(50222).GetBytes(packet);
                        var senddata = this.WrapHL7HeaderTrailer(encodeddata);

                        if (SaveHL7ToLog)
                        {
                            Logger.WriteBinaryToLogfile(senddata, senddata.Length, Logger.Mode.Send);
                        }

                        stream.Write(senddata);
                        Logger.WriteLog("エラーパケット送信完了");
                    }
                    catch (Exception e)
                    {
                        Logger.WriteLog($"ERROR2 ex:{e.Message}", e: e);
                    }
                    stream.Close();
                }
            }
            Logger.WriteLog($"切断されました clientid[{Thread.CurrentThread.ManagedThreadId}{datetime}]");
        }

        private string CreateErrorMessage(QueryPatientInfoResponse response) => response.CreateErrorMessage().SerializeMessage(false);

        private byte[] WrapHL7HeaderTrailer(byte[] body)
        {
            var header = new byte[] { 0x0B };
            var trailer = new byte[] { 0x1c, 0x0d };

            var wrapperBuffer = new byte[body.Length + 3];
            Array.Copy(header, 0, wrapperBuffer, 0, 1);
            Array.Copy(body, 0, wrapperBuffer, 1, body.Length);
            Array.Copy(trailer, 0, wrapperBuffer, 1 + body.Length, 2);

            return wrapperBuffer;
        }

        private byte[] CheckAndExtractRecvHL7Message(byte[] data)
        {
            // ゼロデータを切り取り
            var zeroCount = data.Count(x => x == 0x00);
            var truncatedBuffer = new byte[data.Length - zeroCount];
            Array.Copy(data, truncatedBuffer, data.Length - zeroCount);

            // ヘッダ&トレーラチェック
            if (truncatedBuffer.Length > 4 && (truncatedBuffer[0] != 0x0B || truncatedBuffer[^1] != 0x0D || truncatedBuffer[^2] != 0x1C))
            {
                return null;
            }

            var newBuffer = new byte[truncatedBuffer.Length - 3];
            Array.Copy(truncatedBuffer, 1, newBuffer, 0, truncatedBuffer.Length - 3);
            return newBuffer;
        }
    }
}
