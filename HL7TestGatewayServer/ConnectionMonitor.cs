﻿///////////////////////////////////////////////////////
// Copyright (c) 2020 PHC株式会社
// Filename: ConnectionMonitor.cs
///////////////////////////////////////////////////////
namespace HL7TestGatewayServer
{
    using HL7.Dotnetcore;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Net.Sockets;
    using System.Text;
    using System.Text.Json;
    using System.Threading;
    using System.Threading.Tasks;

    /// <summary>
    /// TCP接続の待ち受けと接続時の処理を担当するクラス
    /// </summary>
    public class ConnectionMonitor
    {
        private const string PersonDataFilename = "PersonList.dat";
        /// <summary>
        /// 待ち受けIPアドレス情報
        /// </summary>
        public IPAddress IpAddress { get; set; }
        /// <summary>
        /// 待ち受けポート番号
        /// </summary>
        public int PortNumber { get; set; }
        /// <summary>
        /// 送受信したHL7データをファイルに保存するかどうかのフラグ
        /// </summary>
        public bool SaveHL7ToLog { get; set; }

        public List<Person> PersonList { get; set; }

        /// <summary>
        /// 実行メソッド
        /// </summary>
        public void Run()
        {
            var listener = new TcpListener(IpAddress, PortNumber);
            listener.Start();

            while (true)
            {
                Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
                var encodings = Encoding.GetEncodings();
                Console.WriteLine($"接続待機中: server[{IpAddress}] port[{PortNumber}].....");

                var client = listener.AcceptTcpClient();

                Console.WriteLine($"接続済: {((IPEndPoint)listener.LocalEndpoint).Address}:{((IPEndPoint)listener.LocalEndpoint).Port} client[{client}]");
                Task.Run(() => CommunicateClient(client));
            }
        }

        private void CommunicateClient(TcpClient client)
        {
            var stream = client.GetStream();
            var datetime = DateTime.Now.ToString("yyyyMMddHHmmss");
            Console.WriteLine($"Starting client[{Thread.CurrentThread.ManagedThreadId}{datetime}]");
            LoadPersonData();

            var bContinue = true;
            // 接続されたら登録情報をすぐに送信する
            while (bContinue)
            {
                try
                {
                    foreach (var person in PersonList)
                    {
                        var response = new VitalCollectResponse
                        {
                            PersonList = new List<Person> { person }
                        };
                        var responsePack = response.CreateResponseMessage();
                        responsePack.HL7Message = responsePack.SerializeMessage(false);
                        Console.WriteLine(responsePack.HL7Message);
                        var packet = responsePack.SerializeMessage(true);

                        Console.WriteLine($"SENDING: {packet.Replace("\r", "\r\n")}");
                        var encodedBuffer = Encoding.ASCII.GetBytes(packet);
                        var senddata = this.WrapHL7HeaderTrailer(encodedBuffer);

                        stream.Write(senddata);

                        // TODO: ACKを5秒待ち
                        byte[] readbuffer = new byte[5012];
                        var bytesRead = stream.Read(readbuffer, 0, readbuffer.Length);
                        if (bytesRead <= 0)
                        {
                            // エラー
                            throw new ArgumentException("ACK受信エラー");
                        }

                        var buffer = CheckAndExtractRecvHL7Message(readbuffer);
                        var ackData = Encoding.ASCII.GetString(buffer).Replace('\0', ' ').Trim();

                        // 解析
                        var message = new Message(ackData);
                        if (!message.ParseMessage())
                        {
                            // エラー
                            throw new ArgumentException("メッセージ解析エラー");
                        }

                        var msa = message.Segments().SingleOrDefault(x => x.Name == "MSA");
                        if (msa == null || msa.Fields(1)?.Value != "AA")
                        {
                            Console.WriteLine("有効なACK受信できず。中断します");
                            bContinue = false;
                            break;
                        }
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine($"エラー発生: {e.Message}");
                    break;
                }
            }
            stream.Close();
            Console.WriteLine($"切断されました clientid[{Thread.CurrentThread.ManagedThreadId}{datetime}]");
        }

        private byte[] WrapHL7HeaderTrailer(byte[] body)
        {
            var header = new byte[] { 0x0B };
            var trailer = new byte[] { 0x1c, 0x0d };

            var wrapperBuffer = new byte[body.Length + 3];
            Array.Copy(header, 0, wrapperBuffer, 0, 1);
            Array.Copy(body, 0, wrapperBuffer, 1, body.Length);
            Array.Copy(trailer, 0, wrapperBuffer, 1+body.Length, 2);

            return wrapperBuffer;
        }

        private byte[] CheckAndExtractRecvHL7Message(byte[] data)
        {
            // ゼロデータを切り取り
            var zeroCount = data.Count(x => x == 0x00);
            var truncatedBuffer = new byte[data.Length - zeroCount];
            Array.Copy(data, truncatedBuffer, data.Length - zeroCount);

            // ヘッダ&トレーラチェック
            if (truncatedBuffer.Length > 4 && (truncatedBuffer[0] != 0x0B || truncatedBuffer[^1] != 0x0D || truncatedBuffer[^2] != 0x1C))
            {
                return null;
            }

            var newBuffer = new byte[truncatedBuffer.Length - 3];
            Array.Copy(truncatedBuffer, 1, newBuffer, 0, truncatedBuffer.Length - 3);
            return newBuffer;
        }

        private void LoadPersonData()
        {
            var jsondata = File.ReadAllText(PersonDataFilename);
            try
            {
                PersonList = JsonSerializer.Deserialize<List<Person>>(jsondata);
            }
            catch (Exception)
            {
                Console.WriteLine($"設定ファイル[{PersonDataFilename}]の読み込みに失敗しました");
            }
        }
    }
}
