﻿///////////////////////////////////////////////////////
// Copyright (c) 2020 PHC株式会社
// Filename: Program.cs
///////////////////////////////////////////////////////
using Microsoft.Extensions.Configuration;
using System.IO;
using System.Net;

namespace HL7TestGatewayServer
{
    internal static class Program
    {
        private const string ConfigFilename = "AppConfig.json";

        private static void Main(string[] args)
        {
            if (args is null)
            {
                throw new System.ArgumentNullException(nameof(args));
            }

            var config = GetConfiguration();

            int.TryParse(config["AppSettings:ServerPortNumber"], out var port);
            var ipAddress = IPAddress.Parse(config["AppSettings:ServerIPAddress"]);

            var monitor = new ConnectionMonitor
            {
                IpAddress = ipAddress,
                PortNumber = port,
                SaveHL7ToLog = bool.Parse(config["AppSettings:SaveHL7Data"])
            };
            monitor.Run();
        }

        private static IConfiguration GetConfiguration()
        {
            return new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile(ConfigFilename)
                .Build();
        }
    }
}
