﻿///////////////////////////////////////////////////////
// Copyright (c) 2020 PHC株式会社
// Filename: VitalCollectResponse.cs
///////////////////////////////////////////////////////
namespace HL7TestGatewayServer
{
    using HL7.Dotnetcore;
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// プロトコル仕様書記載のサンプルデータを再現するようになっています。ORU^R01
    /// </summary>
    public class VitalCollectResponse
    {
        public List<Person> PersonList { get; set; }

        public Message CreateResponseMessage()
        {
            var message = new Message();

            foreach (var person in PersonList)
            {
                AddPersonToMessage(message, person);
            }

            return message;
        }

        private void AddPersonToMessage(Message message, Person person)
        {
            // MSH
            const string alldelimeter = @"|^~\&";
            var todate = DateTime.Now.ToString("yyyyMMddHHmmss");
            var msgstring = $"MSH{alldelimeter}|HL7Gateway|Hospital|VitalCollector|Hospital|{todate}||ORU^R01^ORU_R01|{todate}|P|2.4|||NE|AL||ASCII||ASCII\r";
            var mshmessage = new Message(msgstring);
            mshmessage.ParseMessage();
            message.AddNewSegment(mshmessage.DefaultSegment("MSH"));

            // PID
            var pid = new Segment("PID", new HL7Encoding());
            pid.AddEmptyField();
            pid.AddEmptyField();
            pid.AddNewField(person.PatientID);
            pid.AddEmptyField();
            pid.AddNewField($"{person.LastName}^{person.FirstName}^^^^^L^A");
            pid.AddEmptyField();
            pid.AddNewField(person.BirthDate);
            pid.AddNewField(person.Gender);
            message.AddNewSegment(pid);

            // PV1
            var pv1 = new Segment("PV1", new HL7Encoding());
            pv1.AddEmptyField();
            pv1.AddNewField("I");
            pv1.AddNewField($"^^sbr003^{person.BedIPAddress}:3");
            message.AddNewSegment(pv1);

            // ORC
            var orc = new Segment("ORC", new HL7Encoding());
            orc.AddNewField("RE");
            message.AddNewSegment(orc);

            // OBR & OBX
            var sequence = 1;
            foreach (var obrData in person.OBRElements)
            {
                var obr = new Segment("OBR", new HL7Encoding());
                obr.AddNewField(sequence.ToString());
                sequence++;
                obr.AddEmptyField();
                obr.AddEmptyField();
                obr.AddNewField(obrData.Description);
                obr.AddEmptyField();
                obr.AddEmptyField();
                obr.AddNewField(obrData.ExDatetime);
                obr.AddEmptyField();
                obr.AddEmptyField();
                obr.AddEmptyField();
                obr.AddEmptyField();
                obr.AddEmptyField();
                obr.AddEmptyField();
                obr.AddEmptyField();
                obr.AddEmptyField();
                obr.AddEmptyField();
                obr.AddEmptyField();
                obr.AddEmptyField();
                obr.AddEmptyField();
                obr.AddEmptyField();
                obr.AddNewField("A");
                message.AddNewSegment(obr);

                var obxSequence = 1;
                foreach (var obxData in obrData.OBXElements)
                {
                    var obx = new Segment("OBX", new HL7Encoding());
                    obx.AddNewField(obxSequence.ToString());
                    obxSequence++;
                    obx.AddNewField("NM");
                    obx.AddNewField($"{obxData.Subcode}^{obxData.Description}");
                    obx.AddNewField(obxData.SubID);
                    obx.AddNewField(obxData.Value);
                    obx.AddNewField(obxData.Unit);
                    obx.AddEmptyField();
                    obx.AddEmptyField();
                    obx.AddEmptyField();
                    obx.AddEmptyField();
                    obx.AddNewField(obxData.Final);
                    obx.AddEmptyField();
                    obx.AddEmptyField();
                    obx.AddNewField(obxData.ObservedDate);
                    obx.AddEmptyField();
                    obx.AddEmptyField();
                    obx.AddEmptyField();
                    message.AddNewSegment(obx);
                }
            }
        }
    }

    public class Person
    {
        public string PatientID { get; set; }
        public string BirthDate { get; set; }
        public string Gender { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string BedIPAddress { get; set; }
        public List<OBRElement> OBRElements { get; set; }
    }

    public class OBRElement
    {
        public string Description { get; set; }
        public string ExDatetime { get; set; }
        public List<OBXElement> OBXElements { get; set; }
    }

    public class OBXElement
    {
        public string Subcode { get; set; }
        public string Description { get; set; }
        public string SubID { get; set; } = "1";
        public string Value { get; set; }
        public string Unit { get; set; }
        public string Final { get; set; } = "F";
        public string ObservedDate { get; set; }
    }
}
